%% Solve a nonlinear vector equation 
% In this example a nonlinear vector equation is solved for various values
% of the Newton-Raphson algorithm parameters. The vector equation to be
% solved is comprised from 2 nonlinear equations, and the unknown vector
% comprises from 2 scalars.

%%
% Define function handle f
f = @fun;

%%
% Solve the equation fun(x)=[1;1], with initial point [4;6]:
[xk1,k1] = linesearch(f,[1;1],[4;6],4,1e-5,1000)

%%
%  ____________________________________________________
%  Copyright (c) 2016 by George Papazafeiropoulos
%  Captain, Infrastructure Engineer, Hellenic Air Force
%  Civil Engineer, M.Sc., Ph.D. candidate, NTUA
%  Email: gpapazafeiropoulos@yahoo.gr
%  Website: http://users.ntua.gr/gpapazaf/

