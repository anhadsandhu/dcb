function [xk,k] = linesearch(fun,R,varargin)
%
% Calculation of function root with the quasi-Newton-Raphson method
% accelerated by a line search algorithm
%
% Syntax
%     [#xk#,#k#] = linesearch(#fun#,#R#,#x0#,#kup#,#epsilon#,#maxtol#)
%
% Description
%     The equation fun(x)=R is solved for x, with R not equal to 0. x and R
%     are column vectors of the same size (dim). If the equation fun(x)=0 is
%     to be solved, define R such that fun(x) is replaced by fun(x)+R, so
%     that the equation fun(x)+R=R is solved.
%     The method is implemented as it is described in the material
%     distributed to students of the Analysis and Design of Earthquake
%     Resistant Structures postgraduate course (as in spring 2012)
%     Subject:       Nonlinear Finite Elements
%     Instructor:    Prof. M. Papadrakakis
%     School of Civil Engineering
%     National Technical University of Athens
%
% Input parameters
%     #fun# (function handle): function handle at the left hand side of the
%         equation to be solved. The definition of fun must be of the type:
%         [f,J]=fun(x), where f is the value of fun at x and J is the value
%         of the Jacobian matrix of fun at x. f must be a dim-by-1 vector
%         and J must be a dim-by-dim matrix.
%     #R# ([#m# x 1]): the right hand side of equation #fun#=#R#.
%     #x0# ([#m# x 1]): the initial estimate of the solution #xk#. Default
%         value is #R#.*rand(size(#R#)). If the random number generator is
%         not reset, the existence of convergence problems in a run does not
%         entail that they will exist in a next run.
%     #kup# (scalar): stiffness matrix updater (number of iterations after
%         which the tangent stiffness matrix is updated). For kup=1 the
%         algorithm implemented is Full Newton Raphson. For kup=Inf the
%         algorithm implemented is Modified Newton (initial stiffness).
%         Default value is 4.
%     #epsilon# (scalar): tolerance for convergence of xk and singularity of
%         Jacobian. Default value is 1e-5.
%     #maxtol# (scalar): tolerance for divergence of xk. Default value is
%     1000.
%
% Output parameters
%     #xk# ([#m# x 1]): root of equation #fun#(#xk#)=#R#.
%     #k# (scalar): number of iterations (until convergence).
%
% _________________________________________________________________________
% Copyright (c) 2016
%     George Papazafeiropoulos
%     Captain, Infrastructure Engineer, Hellenic Air Force
%     Civil Engineer, M.Sc., Ph.D. candidate, NTUA
%     Email: gpapazafeiropoulos@yahoo.gr
%     Website: http://users.ntua.gr/gpapazaf/
%



%% Initial checks
% Function handle
if ~isa(fun, 'function_handle')
    error('First input argument is not a function.');
end
% 6 optional inputs at most
numvarargs = length(varargin);
if numvarargs > 6
    error('Too many input arguments.');
end
% Set defaults for optional inputs
optargs = {R.*rand(size(R)) 4 1e-5 1000};
% Skip any new inputs if they are empty
newVals = cellfun(@(x) ~isempty(x), varargin);
% Overwrite the default values by those specified in varargin
optargs(newVals) = varargin(newVals);
% Place optional args in memorable variable names
[x0, kup, epsilon, maxtol] = optargs{:};
% Ensure that R and x0 are not matrices
if all(size(R)>1) || all(size(x0)>1)
    error('The right hand side of the equation or the initial point is a matrix.');
end
% Convert R and x0 to column vectors if either are row vectors
if size(R,2)>1
    R=R';
end
if size(x0,2)>1
    x0=x0';
end
% Check function definition
if size(fun(x0))~=size(x0)
    error('Different size between input and output arguments of the function');
end
% Check sizes of equation and solution
if size(R,1)~=size(x0,1)
    error('Different sizes between right hand side of the equation and initial point.');
end

%% Calculation
% Initialize number of iterations
k = 1;
% Set x[k]=x0 (initial point)
xk = x0;
% Set initial residual error to start the while loop
resk=-R;
% Loop on iterations (k=1 to kmax)
% while any(abs(resk)./abs(R) > epsilon)
while any(abs(resk) ./ abs(R) > epsilon)
    % Calculate KT(x[k]) if it needs to be updated
    if mod(k-1,kup)==0
        [~,KT]=fun(xk);
        % Check if KT(x[k]) is singular
        if abs(det(KT)) < epsilon
            error('Jacobian matrix is singular.');
        end
    end
    % Solve KT(x[k])*u=-res(x[k])
    u = - KT\resk;
    % Calculate x[k+1]=x[k]+u (without line search)
    xk1 = xk + u;
    % Calculate F(x[k+1])
    [Fk1,~]=fun(xk1);
    % Calculate res(x[k+1])
    resk1 = Fk1-R;
    % Form and solve quadratic equation ��(x)=a*r(a)=a*(c1*a^2+c2*a+c3)=0
    % Calculate r(0)
    r0=u'*resk;
    % Calculate r(1)
    r1=u'*resk1;
    % Calculate eta=r(0)/r(1)
    eta=r0/r1;
    % Solve quadratic equation
    if eta>0 && eta<=4 % discriminant<0
        % Take the root of the derivative to minimize quadratic equation
        alpha=eta/2;
    elseif eta==0 % discriminant=0
        disp('Algorithm has converged');
        break
    else % discriminant>0
        % Take the larger of the two real solutions
        alpha=eta/2+sqrt(eta^2/4-eta);
    end
    if alpha>1
        % Update x[k] to x[k+1] (with line search)
        xk=xk+alpha*u;
        % Calculate F(x[k+1])
        [Fk,~]=fun(xk);
        % Calculate res(x[k+1])
        resk = Fk-R;
        % % Calculate r(alpha)
        % ralpha=u'*resk;
    else
        % Update x[k] to x[k+1] (without line search)
        xk=xk1;
        % Calculate F(x[k+1])
        [Fk,~]=fun(xk);
        % Calculate res(x[k+1])
        resk = Fk-R;
    end
    % Check for divergence
    if any(abs(resk)./abs(R)) > maxtol
        error(['Solution diverges.Successful iterations = ',num2str(k-2)]);
    end
    % Update iteration number k
    k = k + 1;
    max(abs(resk) ./ abs(R))
    % [max(abs(resk)./abs(R))]
end
