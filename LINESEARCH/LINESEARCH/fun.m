function [f,J]=fun(x)
%
% Arbitrary function definition for solution of a given equation 
% 
% Syntax
%     [#f#,#J#]=fun(#x#)
%
% Description
%     This function calculates an output vector and a Jacobian matrix which
%     are used by the linesearch function to find the roots of the
%     equation associated with this function. After having defined the
%     first output of the function, which is simply its value, the jacobian
%     can be symbolically calculated by executing the following commands in
%     MATLAB:
%     syms x1 x2
%     J=jacobian(fun([x1;x2]));
%     Jf=matlabFunction(J);
%     
% Input parameters
%     #x# ([#m# x 1]) is an arbitrary vector, belonging at the domain of
%         definition of the function.
% 
% Output parameters
%     #f# ([#m# x 1]) is the value of the vector-valued function, at #x#.
%     #J# ([#m# x #m#]) is the jacobian of the function at #x#.
%
% _________________________________________________________________________
% Copyright (c) 2016
%     George Papazafeiropoulos
%     Captain, Infrastructure Engineer, Hellenic Air Force
%     Civil Engineer, M.Sc., Ph.D. candidate, NTUA
%     Email: gpapazafeiropoulos@yahoo.gr
%     Website: http://users.ntua.gr/gpapazaf/
%



% Function output column vector (2-by-1)
f1 = x(1)^2 + x(2)^2 - 49;
f2 = x(1)*x(2) -24;
f = [f1;f2];
% Function Jacobian output matrix (2-by-2)
J=[ 2*x(1), 2*x(2);
   x(2),   x(1)];

end
