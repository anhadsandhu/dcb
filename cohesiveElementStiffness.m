function [Ke_all] = cohesiveElementStiffness(msh,u,model,postprocess,Ke_all)

    Ke_cohesive = zeros(msh.nedof^2,msh.nelem);
    N = msh.coh.N;
    B = zeros(3,24);
    
    mixedmode_form = true;
    
    for el = 1 : msh.nelem
        if(~msh.elements{el}.continuum) % if cohesive element
            
            Ke = zeros(24);
            Ke2 = Ke;
            ulocal = u(msh.e2g(el,:));
            coords = msh.coords(msh.elements{el}.connectivity,:);
            
            area = (coords(7,1) - coords(1,1)) * (coords(7,2) - coords(1,2));% * (coords(7,3) - coords(1,3))
            factor = area / 4; % because area of reference cohesive element is 4
            for ip = 1 : msh.coh.nip

                B(1,1:4) = -N{ip}; B(1,5:8) = N{ip};
                B(2,9:12) = -N{ip}; B(2,13:16) = N{ip};
                B(3,17:20) = -N{ip}; B(3,21:24) = N{ip};

                
                % J = coords'* msh.dN{ip};
                
                Delta = B * ulocal;

                if (mixedmode_form)
                    [Dsr,scalar,loading] = bilinearLaw(Delta,model,postprocess,ip,el);
                else % pure mode formulation
                    [Dsr,scalar,loading] = bilinearLaw_uncoupled(Delta,model,postprocess,ip,el);
                end
                
                Ke = Ke + B'*Dsr*B*msh.coh.ip.wgts(ip)*factor;
                
                if (mixedmode_form)
                    % for d = 1 : 3
                    if (loading == 1) % softening
                        si = ones(3);
                        if(Delta(3) <= 0)
                            si(:,3) = 0;
                            si(3,:) = 0;
                        end
                        S = ((Delta * Delta') .* si .* scalar);
                        % S = sum(sum(S));
                        Ke2 = Ke2 + (B' * S * B);
                    end
                else % pure mode formulation
                    for d = 1 : 3
                        if (loading == 1) % softening
                            si = ones(3);
                            if(Delta(3) <= 0)
                                si(:,3) = 0;
                                si(3,:) = 0;
                            end
                            S = ((Delta(d) * Delta(d)') .* si .* scalar(d));
                            % S = sum(sum(S));
                            Ke2 = Ke2 + (B' * S * B);
                        end
                    end
                end
            end
            % end	% for each ip

            % Ke = 0.5*(Ke + Ke');
            
            % if sum(sum(abs(Ke-Ke'))) > 1e-4
            %     error('Element Stiffness Matrix Not Symmetric');
            % end
            Ke = Ke - Ke2;
            Ke = Ke(:); % Convert to a column vector
            Ke_cohesive(:,el) = Ke;
            Ke_all(:,el) = 0;
        end
    end

    Ke_all = Ke_all + Ke_cohesive;
    % K_c = sparse(Kindx.i',Kindx.j',Ke_cohesive);
    % if (isscalar(K_c0))
    %     KNL = K_c;
    % else
    %     KNL = K_c - K_c0;
    % end
    
    % K = KL + KNL;
    % K = sparse(Kindx.i',Kindx.j',Ke_all);

end