function [globdata,settings] = riksSolver2(msh,globdata, bnds, free, u, u_control,funhandle,settings)
    
    %% Setting variables being used in this script
    % I typically store these variables in a structure that is passed to the
    % this function as they control its behaviour. I've called this "settings"
    % here
    
    
    %{
      Note: I generally define a structure, which I've called globaldata here,
      that contains the current state of the structure (displacement, loads)
      and general information. You'll obviously have to adapt this.
    %}
    
    %% Code  

    if (u_control)% you are applying displacements rather than forces
        u_c = globdata.uhatf;%vector of constrained displacements;
    else
        u_c = [];%empty vector;
    end
    dofs_c = bnds.fixed;% constrained degrees of freedom relating to u_c;

    if settings.flag == 0 % if previous run of this script converged
        globdata.cycle = globdata.cycle + 1; % increase load step by 1

        u = globdata.state; % current displacement vector
        param1 = globdata.param1; % current loading parameter

        % Store path values
        settings.uPrev = u;
        settings.param1Prev = param1;

        fprintf('===========================\n')
        fprintf(['Load step: ', num2str(globdata.cycle),'\n'])
        fprintf('===========================\n')
        fprintf('NR iter: L2-norm residual\n')
    elseif settings.flag == 1 % try same load step again
        u = globdata.state;
        param1 = globdata.param1;

        settings.flag = 0;
    end

    err = 1;
    iiter = 0;

    % Predictor step
    if globdata.cycle == 1 % for the very first step of an analysis
                           % assemble tangent stiffness and internal load vector, which has
                           % been factorised to include only the portion that contains the
                           % free DOFs without boundary conditions
        [~,~,Kt] = funhandle(globdata.state,zeros(msh.tdof,1));

        Dparam1_1 = globdata.initStep; % first step of loading parameter

        f = -Dparam1_1 * Kt(free,bnds.fixed) * u_c;

        Du1f = Kt(free,free)\f; % find displacement increment of free DOFs
        
        % I generally define a matrix F that when multiplied by the
        % displacement vector without the boundary DOFs (e.g. Du1f) just 
        % adds zero to that vector where the boundary dofs should be
        Du1 = globdata.F*Du1f; 
        % then add in the values at the boundary DOFs
        Du1(dofs_c) = Dparam1_1*u_c;

        settings.DuPrev = Du1; %save
        settings.Dparam1Prev = Dparam1_1; %save
    else % for all other steps
        [~,~,Kt] = funhandle(globdata.state,zeros(msh.tdof,1));

        duf = Kt(free,free)\(-Kt(free,bnds.fixed) * u_c);

        fac = settings.initStep/abs(settings.initStep); % direction of first loading parameter
        
        % Compute parameter increment from arc-length
        Dparam1_1 = fac * settings.Ds / sqrt(duf'*duf + settings.beta^2);
        % Logic for limit point traversal
        if dot(globdata.F'*settings.DuPrev,duf) > 0
            Dparam1_1 = abs(Dparam1_1);
        else
            Dparam1_1 = -abs(Dparam1_1);
        end
        Du1f = Dparam1_1*duf;
        Du1 = globdata.F*Du1f; Du1(dofs_c) = Dparam1_1*u_c;
    end

    % Update displacements
    u = u + Du1; globdata.state = u;
    Du = Du1; globdata.Dstate = Du;
    Duf = Du1f;

    % Update loading parameter
    param1 = param1 + Dparam1_1; globdata.param1 = param1;
    Dparam1 = Dparam1_1; globdata.Dparam1 = Dparam1;

    % Corrector step
    [fint,fintf,Kt] = funhandle(globdata.state,zeros(msh.tdof,1));

    % get the externally applied force vector acting on the free DOFs
    fextf = zeros(size(fintf));
    % the residual
    res = fintf - fextf;
    
    while err > settings.tol
        iiter = iiter + 1;

        % Compute correcter response from the residual and the applied load
        f = [-res -Kt(free,bnds.fixed)*u_c];
        du12f = Kt(free,free)\f;
        du1f = du12f(:,1); du2f = du12f(:,2); % split in two

        % Compute corrector to the loading parameter
        if globdata.cycle == 1
            dparam1 = 0; % standard Newton-Raphson for first step only
        else
            % Riks arc-length equation
            dparam1 = -(Du1f'*du1f)/(Du1f'*du2f+settings.beta^2*Dparam1_1);
        end

        % Update displacements
        duf = du1f + dparam1*du2f;
        du = globdata.dofs.F*duf; du(dofs_c) = dparam1*u_c;

        Du = Du + du; globdata.Dstate = Du;
        u = u + du; globdata.state = u;
        Duf = Duf + duf;
        
        % Update loading parameter
        Dparam1 = Dparam1 + dparam1; globdata.Dparam1 = Dparam1;
        param1 = param1 + dparam1; globdata.param1 = param1;  

        [fint,fintf,Kt] = funhandle(globdata.state,zeros(msh.tdof,1));

        % External forces acting on free nodes
        % fextf = calcfextf(globdata); 
        % Define a value to compare residual against
        errNorm = norm(fextf); errNorm(errNorm < 1) = 1;
        % New residual
        res = fintf - fextf; resNorm = norm(res);
        % Compute weighted error
        err = resNorm / errNorm;

        % Catching NaN in err
        fprintf(['Iter ',num2str(iiter),' : ',num2str(err),'\n'])
        if isnan(err)
            error('Solver broke. Quitting.');
        end

        % If we don't converge after reaching the iterative limits we
        % reduce the step size and just try again
        if iiter == settings.maxIter && err > settings.tol
            fprintf('Newton-Raphson iterations did not converge! Reducing step size.\n');

            % Computing a reduction factor between 0.1 and 0.75
            redFac = settings.tol/err;
            if redFac < 0.1
                redFac = 0.1;
            elseif redFac > 0.75
                redFac = 0.75;
            end

            % apply reduction factor
            if globdata.cycle == 1 % for first step only
                settings.initStep = redFac * settings.initStep;
            else
                % Check that new arc-length isn't too big/small
                settings.Ds = redFac * settings.Ds;
                if abs(settings.Ds) > abs(settings.DsMax)
                    settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMax;
                elseif abs(settings.Ds) < abs(settings.DsMin)
                    settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMin;
                end  
            end

            % set flat to 1 to denote that we didn't converge
            settings.flag = 1;
            % I typically break to go out of the function and back in again
            % but you might want to change that
            break 
        end
    end
    fprintf('\n');

    if settings.flag == 0 % if converged

        globdata.converged = 1; % I use this to do other stuff in FE code

        globdata.fint = fint; %save
        globdata.Kt = Kt; %save

        settings.DuPrev = Du; %save
        settings.Dparam1Prev = Dparam1; %save

        if globdata.cycle == 1 % for first step compute arc-length Ds
            settings.Ds = sqrt(Duf'*Duf + settings.beta^2*Dparam1^2);
            if abs(settings.Ds) > abs(settings.DsMax)
                settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMax;
            elseif abs(settings.Ds) < abs(settings.DsMin)
                settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMin;
            end
            settings.DsPrev = settings.Ds;
        else % for all other steps automatically decrease/increase arc-length or keep fixed 
            if settings.fixedStep == 0 % increase/decrease based on nr. of iterations
                settings.DsPrev = settings.Ds;
                settings.Ds = settings.DsPrev * (settings.optIter/iiter)^settings.damp;
                if abs(settings.Ds) > abs(settings.DsMax)
                    settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMax;
                elseif abs(settings.Ds) < abs(settings.DsMin)
                    settings.Ds = (settings.Ds/abs(settings.Ds)) * settings.DsMin;
                end  
            elseif settings.fixedStep == 1 && settings.DsPrev ~= settings.Ds % keep Ds fixed
                settings.Ds = settings.DsPrev;
            end
        end

        % if we have reached limits of analysis then quit
        if (globdata.cycle > settings.maxCycle) || (globdata.param1 > settings.maxParam1) || (globdata.param1 < settings.minParam1)
            globdata.active = 0; % global analysis variable
        end

    elseif settings.flag == 1 % if we haven't converged re-set values to the values at the beginning of the attempt
        globdata.converged = 0;
        globdata.state = settings.uPrev;
        globdata.Dstate = settings.DuPrev;
        globdata.param1 = settings.param1Prev;
        globdata.Dparam1 = settings.Dparam1Prev;
    end
end
