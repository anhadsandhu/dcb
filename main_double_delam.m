addpath('LINESEARCH/LINESEARCH/');
close all;
clear all;

% make materials
model.E_R = 0.5; %    GPa
model.nu_R = 0.4;

model.E1 = 115; %    GPa
model.E2 = 8.5; %    GPa
model.E3 = 8.5; %    GPa

model.nu_12 = 0.29; 
model.nu_13 = 0.29;
model.nu_23 = 0.3;

model.G_12 = 4.5;   %   GPa
model.G_13 = 4.5;   %   MPa
model.G_23 = 3.7; %   GPa

model.T = 7e-3;
model.S = 7e-3;
model.N = 3.3e-3;
model.GIc = 0.33e-3;
model.GIIc = 0.8e-3;
model.GIIIc = 0.8e-3;
model.K = 1e3;


[isotropic,composite] = makeMaterials(model);
ss = [0,-1,0,-1,0]; % -1 is interface
layer_colours = [2.5,2.5,0,7.5,7.5];
% read mesh
msh = readMesh('cze.msh','HEXAS');

L = max(msh.coords);

% integrations points and shape functions
[msh,msh.coh] = defineIPs(msh);
[msh.N,msh.dN,msh.coh] = ShapeFunctions(msh,msh.coh);
% element stiffness matrix

indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

Ke_all = zeros(msh.nedof^2,msh.nelem);

regions.name = 'regions';
regions.data = zeros(msh.nelem,1);
numCohesive = 0; % counter for number of cohesive elements
for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
        msh.elements{ie}.continuum = 1;
    else
        C = isotropic;
        ang = 0;
        msh.elements{ie}.continuum = 0;
        numCohesive = numCohesive + 1;
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    
    if(msh.elements{ie}.continuum == 1)
        Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    end
    
end
global Dmax;
Dmax = zeros(msh.coh.nip,msh.nelem);
for el = 1 : msh.nelem
    cen = mean(msh.coords(msh.elements{el}.connectivity,:));
    if (cen(3) < L(3)/2)
        if (cen(1) >= 20 & cen(1) <= 40 & ~msh.elements{el}.continuum)
            Dmax(:,el) = 10;
        end
    else
        if (cen(1) >= 60 & ~msh.elements{el}.continuum)
            Dmax(:,el) = 10;
        end
    end
    
end
% nonlinear
% myhandle = @(u)cohesiveElementStiffness(msh,u,model,false,Ke_all,Kindx);
% Ke_all = Ke_all + Ke_cohesive;

% apply BCs
leftbnd = find(msh.coords(:,1) < 1e-6);
topbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) > L(3)-1e-6);
botbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) < 1e-6);
lbnd = [leftbnd; leftbnd + msh.nnode; leftbnd + msh.nnode * 2];
tbnd = [topbnd + msh.nnode * 2];
bbnd = [botbnd + msh.nnode * 2];

disp_bnd = union(tbnd,bbnd); % dofs under displacement control
disp_bnd = union(lbnd,disp_bnd); % dofs under displacement control

% topbnd = find(msh.coords(:,1) >  150-1e-6 & msh.coords(:,3) > 2.98-1e-6);
% botbnd = find(msh.coords(:,1) >  150-1e-6 & msh.coords(:,3) < 1e-6);

bnds.fixed = disp_bnd;%union(disp_bnd,msh.nnode+1:msh.nnode*2);
bnds.disp.top = topbnd + msh.nnode*2;
bnds.disp.bot = botbnd + msh.nnode*2;

% app_disp = 1e-1;
% f = zeros(msh.tdof,1);
% u = zeros(msh.tdof,1);
% f(bnds.disp.top) = app_disp/2;
% f(bnds.disp.bot) = -app_disp/2;
% u = f;

fun = @(u,f)myfun(msh,u,f,model,Ke_all,Kindx,bnds,false);

% [res0,K0] = fun(u);

% newton solver (without line search)
app_disp = 1e-3;
epsilon = 1e-6;
u_old = zeros(msh.tdof,1);
i = 1;
conv = false;
while (app_disp < 30)
    if (i > 1)
        app_disp = app_disp + (0.5 * sqrt(10/cnt));
    end
    f = zeros(msh.tdof,1);
    f(bnds.disp.top) = app_disp/2;
    f(bnds.disp.bot) = -app_disp/2;

    conv = false;
    cnt = 0;
    while (true)
        u_old(bnds.disp.top) = app_disp/2;
        u_old(bnds.disp.bot) = -app_disp/2;
        [res,K] = fun(u_old,f);
        % norm(res)
        if (norm(res) < epsilon)
            conv = true;
            break;
        end
        u_new = u_old - (K \ res);
        u_old = u_new;
        cnt = cnt + 1;
    end
    
    
    % post process
    if (conv)
        postprocess = true;
        bnds_free.fixed = [];
        [~,Kfree] = myfun(msh,u_new,f,model,Ke_all,Kindx,bnds_free,postprocess);
        force = Kfree * u_new;
        F(i) = sum(abs(force));
        figure(1)
        hold on;
        plot(app_disp,F(i),'or');
        
        % det(K)
        % eigs(K,[],5,'smallestabs')
        
        [i app_disp F(i) cnt]
        
        disp(i) = app_disp;

        u = u_new;
        vector.name = 'U';
        vector.data = zeros(msh.nnode,3);

        vector.data(:,1) = u(1:msh.nnode);
        vector.data(:,2) = u(msh.nnode+1:2*msh.nnode);
        vector.data(:,3) = u(2*msh.nnode+1:3*msh.nnode);

        output_file = strcat('sol',num2str(i),'.vtk');

        output_title = strcat('sol.vtk ');

        matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)
        i = i + 1;
    end
end
