function [u,lambda,iter,Aout,DeltaSout,D,Fr] = crisfield_arc_length(functn,globdata,msh,free,bnds,max_disp,varargin)


    optargs = {'sph' 400 0 1 20 4 5};
    % skip any new inputs if they are empty
    newVals = cellfun(@(x) ~isempty(x), varargin);
    % overwrite the default values by those specified in varargin
    optargs(newVals) = varargin(newVals);
    % place optional args in memorable variable names
    [Crisver,nmax,lambda0,Deltalambdabar,imax,Id,tol] = optargs{:};
    u0 = globdata.state;

    if strcmp(Crisver,'cyl')
        w=0;
    elseif strcmp(Crisver,'sph')
        w=1;
    else
        error('Incorrect type of Crisfield method.')
    end
    D = 0; % total displacement boundary condition
    maxS = 5e-2;
    minS = 1e-6;
    app_disp = 1e-3/2;

    %% Calculation
    % initialize variables
    DeltaSout=zeros(1,nmax);
    lambda=zeros(1,nmax);
    u=zeros(length(u0),nmax);
    iter=zeros(1,nmax);
    Aout=zeros(1,nmax);
    % loop on steps (p=1 to nmax), Fig.(8) in [3]
    % for p=1:nmax
    p = 0;
    i=0;
    while D < max_disp
        p = p + 1;
        if p==1
            % set lambda=lambda0+Deltalambdabar
            lambdap=lambda0+Deltalambdabar; % Fig.(8) in [3]
            lambdai=lambda0+Deltalambdabar; % Fig.(8) in [3]
            up=u0;
            ui=u0;
            R = ones(msh.tdof,1);
        end
        % loop on iterations (i=0 to imax), Fig.(8) in [3]
        i = 0;
        while (true)% i < imax
            % update stiffness
            [~,K,V,msh] = functn(ui,zeros(msh.tdof,1),false);
            FF = 1 : size(V,2);
            bmodes = (size(V,2) - msh.m + 1);
            FB = bmodes;%:size(V,2);
            % FF(FB) = [];
            % app_disp_top = mean(V(bnds.disp.top,FB));
            % app_disp_bot = mean(V(bnds.disp.bot,FB));
            % V(bnds.disp.top,FB) = repmat(app_disp_top,size(bnds.disp.top,1),1);
            % V(bnds.disp.bot,FB) = repmat(app_disp_bot,size(bnds.disp.bot,1),1);
            
            % coarse problem
            KT = V' * K * V;
            
            %% tie constraints
            [lovlp,ial,ibl] = intersect(msh.Omg{1}.nodes,msh.Omg{2}.nodes);
            % lovlp = setdiff(lovlp,msh.Omg{ip-1}.procbnd);
            [rovlp,iar,ibr] = intersect(msh.Omg{3}.nodes,msh.Omg{2}.nodes);
            % rovlp = setdiff(rovlp,msh.Omg{ip+1}.procbnd);
            ovlp = union(lovlp,rovlp);
            interior_nodes = setdiff(msh.Omg{2}.nodes,ovlp); % get ids of nodes shared between R and F subdomains
            xc = msh.coords(interior_nodes,1);
            idsl = find(xc < (min(xc)+1e-6));
            idsr = find(xc > (max(xc)-1e-6));
            % dofsl = [idsl; idsl + 344; idsl + 344 + 344]+1;
            % dofsr = [idsr; idsr + 344; idsr + 344 + 344]+1;
            % for id = 1 : length(dofsl)
            %     KT(dofsl(id),:) = 0;
            %     KT(dofsl(id),dofsl(id)) = -1;
            %     KT(dofsl(id),1) = 1;
            % end
            % for id = 1 : length(dofsr)
            %     KT(dofsr(id),:) = 0;
            %     KT(dofsr(id),dofsr(id)) = -1;
            %     KT(dofsr(id),FB) = 1;
            % end
            % apply tie constraints in the fine ui vector. These  are the indices to be constrained
            idsl = interior_nodes(idsl);
            idsr = interior_nodes(idsr);
            dofsl = [idsl; idsl + msh.nnode; idsl + msh.nnode + msh.nnode];
            dofsr = [idsr; idsr + msh.nnode; idsr + msh.nnode + msh.nnode];
            lids = [msh.Omg{1}.procbnd; msh.Omg{1}.procbnd + msh.nnode; msh.Omg{1}.procbnd + 2*msh.nnode];
            rids = [msh.Omg{3}.procbnd; msh.Omg{3}.procbnd + msh.nnode; msh.Omg{3}.procbnd + 2*msh.nnode];

            % DUNE style BCs
            uc = V' * ui;
            % uc(FB) = lambdai;
            KT(FB,:) = 0;
            KT(FB,FB) = 1;
            R = KT * uc;

            DeltauRi = V * (-KT \ R);
            DeltauRi(dofsl) = DeltauRi(lids);
            DeltauRi(dofsr) = DeltauRi(rids);

            R = V * R;
            R(bnds.fixed) = 0;
            F = zeros(length(KT),1);
            F(FB) = -lambdai;% * app_disp;

            DeltauFi = V * (-KT \ F);
            DeltauFi(dofsl) = DeltauFi(lids);
            DeltauFi(dofsr) = DeltauFi(rids);

            notconv = true;
            
            
            % if(msh.Omg{msh.numDomain - 1}.damage_state ~= -1)
            %     error('Crack propagated to next subdomain. Stopping simulation.');
            % end
            
            if i==0
                if p==1
                    DeltaS=Deltalambdabar*sqrt(w+DeltauFi'*DeltauFi); 
                    A=0;
                    Deltaui=Deltalambdabar*DeltauFi;
                    ui=ui+Deltaui; 
                else
                    DeltaS=DeltaS*sqrt(Id/ilast);
                    if (DeltaS > maxS)
                        DeltaS = maxS;
                    end
                    if (DeltaS < minS)
                        DeltaS = minS;
                    end
                    A=(up-up_1)'*DeltauFi; 
                    Deltalambdai=sign(A)*DeltaS/sqrt(w+DeltauFi'*DeltauFi); 
                    lambdai=lambdap+Deltalambdai;
                    Deltaui=DeltauRi+Deltalambdai*DeltauFi;
                    ui=ui+Deltaui;
                end
            else
                % solve: a*Deltalambdai^2 + 2*b*Deltalambdai + c = 0
                vi=ui-up+DeltauRi;
                a=DeltauFi'*DeltauFi;
                b=DeltauFi'*vi;
                c=vi'*vi-DeltaS^2;
                upi=ui-up;
                if (b^2-4*a*c) > 0
                    Deltalambdai=[(-b-sqrt(b^2-4*a*c))/a,(-b+sqrt(b^2-4*a*c))/a]; Deltalambdai = Deltalambdai ./ 2;
                    Deltaui=[DeltauRi+Deltalambdai(1)*DeltauFi,DeltauRi+Deltalambdai(2)*DeltauFi];
                    ui1=[ui+Deltaui(:,1),ui+Deltaui(:,2)];
                    upi1=[ui1(:,1)-up,ui1(:,2)-up];
                    cosines=[upi'*upi1(:,1)/abs(upi'*upi1(:,1)),upi'*upi1(:,2)/abs(upi'*upi1(:,2))];
                    % root selection (maximum of the two cosines)
                    [~,IX]=sort(cosines);
                    Deltalambdai=Deltalambdai(IX(2));
                    Deltaui=Deltaui(:,IX(2));
                    ui=ui1(:,IX(2));
                else % take the global optimum
                    Deltalambdai=-b/(2*a);
                    % if(abs(Deltalambdai) > maxD)
                    %     Deltalambdai = sign(Deltalambdai)*maxD;
                    % end
                    Deltaui=DeltauRi+Deltalambdai*DeltauFi;
                    ui=ui+Deltaui;
                end

                lambdai=lambdai+Deltalambdai;
                
                % fprintf(['p = ',num2str(p),', i = ',num2str(i),', |R| = ',num2str(norm(R(free))),'\n']);%[p i norm(R(free))]);
                fprintf(['p = ',num2str(p),', i = ',num2str(i),', |R| = ',num2str(norm(R(free))),'\n']);
                
                % convergence test
                % norm(R(free))
                if (norm(R(free)) < tol)
                    notconv = false;
                    [~,~,~,~] = functn(ui,zeros(msh.tdof,1),true); % update damage if converged
                    msh = subdomainDamage(msh);
                    break; 
                end
            end
            
            
            
            % if solver does not converge in imax, reduce step size by a factor of 10
            if(notconv & i >= imax-1)
                if (abs(Deltalambdai) < 1e-15)
                    error('Step size too small');
                end
                fprintf(['Cutting step = ',num2str(Deltalambdai),'\n']);
                % lambdai = lambdap + Deltalambdai/10;
                lambdai = lambdai - (0.9*Deltalambdai);
                Deltalambdai = Deltalambdai/10;
                % ui = up + ui./10;% + ui./10;
                % up_1 = up + ui./10;% + ui./10;
                i = 0;
            end
            % update iteration number
            i=i+1;
            
        end % end while i
        
        % set initial values for the next increment
        lambdap=lambdai;
        up_1=up;
        up=ui;
        % number of iterations of the last increment
        ilast=i;
        % Various output results
        lambda(p)=lambdap;
        u(:,p)=up;
        iter(p)=i;
        Aout(p)=A;
        DeltaSout(p)=DeltaS;
        
        % plotter
        D(p) = mean(up(bnds.disp.top)) * 2;
        % [D(p) DeltaS lambdap Deltalambdai]
        % D(p) = lambdap*1e-2;
        % V(:,FB) = V(:,FB) * lambdai;
        % force = KT(FB,FB)*(V(bnds.fixed,FB)' * up(bnds.fixed));%(V * KT * V') * up;
        % KT(:,FB) = KT(:,FB) * lambdai;
        % KT(FB,:) = KT(FB,:) * lambdai;
        KKT = (V(:,:) * KT(:,:) * V(:,:)');
        force = KKT * up;
        % force =  KT * (V' * up);
        % force = ;
        % Fr(p) = sum(abs(force(bnds.disp.top)));
        Fr(p) = sum(abs(force(bnds.fixed))) * lambdai;
        figure(100)
        hold on;
        box on;
        grid on;
        plot(D(p),Fr(p),'ok','MarkerSize',3);
        
        
        % vtk output
        vector.name = 'U';
        vector.data = zeros(msh.nnode,3);

        vector.data(:,1) = up(1:msh.nnode);
        vector.data(:,2) = up(msh.nnode+1:2*msh.nnode);
        vector.data(:,3) = up(2*msh.nnode+1:3*msh.nnode);

        output_file = strcat('sol',num2str(p),'.vtk');

        output_title = strcat('sol.vtk ');
        regions.name = 'Regions';
        regions.data = ones(msh.nelem,1);
        matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)
        
        % vtk output
        vector.name = 'F';
        vector.data = zeros(msh.nnode,3);

        vector.data(:,1) = force(1:msh.nnode);
        vector.data(:,2) = force(msh.nnode+1:2*msh.nnode);
        vector.data(:,3) = force(2*msh.nnode+1:3*msh.nnode);

        output_file = strcat('force',num2str(p),'.vtk');

        output_title = strcat('force.vtk ');
        regions.name = 'Regions';
        regions.data = ones(msh.nelem,1);
        matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)

    end % end while d


end