addpath('LINESEARCH/ARC_LENGTH/ARC_LENGTH/');
% close all;
clear all;

% make materials
model.E_R = 0.5; %    GPa
model.nu_R = 0.4;

model.E1 = 150; %    GPa
model.E2 = 11; %    GPa
model.E3 = 11; %    GPa

model.nu_12 = 0.25; 
model.nu_13 = 0.25;
model.nu_23 = 0.45;

model.G_12 = 6;   %   GPa
model.G_13 = 6;   %   MPa
model.G_23 = 3.7; %   GPa

model.T = 5e-3;
model.S = 5e-3;
model.N = 5e-3;
model.GIc = 0.268e-3;
model.GIIc = 0.268e-3;
model.GIIIc = 0.268e-3;
model.K = 1e0;

model.alpha = 1; % power law criterion. Set this to 0 to use B-K criterion
model.eta = 0; % B-K criterion

crackLen = 55;


[isotropic,composite] = makeMaterials(model);
% ss = [0,-1,0]; % -1 is interface
ss = [0,0,-1,0,0]; % -1 is interface
% layer_colours = [2.5,0,7.5];
layer_colours = [1.5,2.5,0,7.5,8.5];
% read mesh
msh = readMesh('dcb.msh','HEXAS');

L = max(msh.coords);

% integrations points and shape functions
[msh,msh.coh] = defineIPs(msh);
[msh.N,msh.dN,msh.coh] = ShapeFunctions(msh,msh.coh);
% element stiffness matrix

indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

Ke_all = zeros(msh.nedof^2,msh.nelem);

regions.name = 'regions';
regions.data = zeros(msh.nelem,1);
numCohesive = 0; % counter for number of cohesive elements
for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
        msh.elements{ie}.continuum = 1;
    else
        C = isotropic;
        ang = 0;
        msh.elements{ie}.continuum = 0;
        numCohesive = numCohesive + 1;
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    

    if(msh.elements{ie}.continuum == 1)
        Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    end
    
end
global Dmax1;
global Dmax2;
global Dmax3;
Dmax1 = zeros(msh.coh.nip,msh.nelem);
Dmax2 = Dmax1;
Dmax3 = Dmax1;
for el = 1 : msh.nelem
    cen = mean(msh.coords(msh.elements{el}.connectivity));
    if (cen(1) >= (L(1) - crackLen) & ~msh.elements{el}.continuum)
        Dmax1(:,el) = 1e6;
        Dmax2(:,el) = 1e6;
        Dmax3(:,el) = 1e6;
    end
end
% nonlinear
% apply BCs
leftbnd = find(msh.coords(:,1) < 1e-6);
topbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) > L(3)-1e-6);
botbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) < 1e-6);
lbnd = [leftbnd; leftbnd + msh.nnode; leftbnd + msh.nnode * 2];
% tbnd = [topbnd; topbnd + msh.nnode; topbnd + msh.nnode * 2];
bbnd = [botbnd + msh.nnode * 2];

disp_bnd = bbnd;%union(tbnd,bbnd); % dofs under displacement control
disp_bnd = union(lbnd,disp_bnd); % dofs under displacement control

bnds.fixed = disp_bnd;%union(disp_bnd,msh.nnode+1:msh.nnode*2);
% bnds.disp.top = topbnd + msh.nnode*2;
bnds.disp.bot = botbnd + msh.nnode*2;

free = 1 : msh.tdof;
free(bnds.fixed) = [];

%% settings for Riks Solver
settings.tol = 1e-3; % convergence tolerance
settings.optIter = 4; % target number of iterations for convergence
settings.damp = 2; % exponent controlling automatic increase/decrease of arc-length
settings.maxIter = 10; % maximum nr of iterations before cutting step size
settings.DsMax = 10; % maximum allowable arc-length increment
settings.DsMin = 1e-15; % minimum allowable arc-length increment
settings.initStep = 1; % first increment of loading parameter
settings.fixedStep = 0; % toggle to allow arc-length to adapt or remain fixed     
settings.maxCycle = 1e3; % maximum allowable steps
settings.maxParam1 = 1e2; % maximum allowable loading parameter
settings.minParam1 = -1e2; % minimum allowable loading parameter
settings.beta = 0; % parameter to control arc-length equation

settings.flag = 0; % variable to check if previous step converged
settings.Ds = 0; % current arc-length
settings.DsPrev = 0; % arc-length of previous step

settings.nrDofs = msh.tdof; % nr of degrees of freedom in your unknown displacement vector
settings.uPrev = zeros(settings.nrDofs,1); % displacement vector of previous converged step
settings.DuPrev = zeros(settings.nrDofs,1); % displacement increment vector of previous converged step
settings.param1Prev = 0; % loading parameter value of previous step
settings.Dparam1Prev = 0; % loading parameter increment of previous step
%% global data
globdata.cycle = 0;
globdata.state = zeros(msh.tdof,1);
globdata.param1 = 0;
globdata.Dparam1 = 0;
globdata.initStep = settings.initStep;
globdata.F = zeros(msh.tdof,length(free));
for i = 1 : length(free)
    globdata.F(free(i),i) = 1;
end
globdata.dofs.F = globdata.F;

app_disp = 1e-3; % constant displacement boundary condition. Actual displacement = param1*app_disp

f = zeros(msh.tdof,1);
% f(bnds.disp.top) = app_disp/2; % DCB boundary conditions
% f(bnds.disp.bot) = -app_disp/2;
% u_old = f;
% u_old(bnds.disp.top) = app_disp/2;
% u_old(bnds.disp.bot) = -app_disp/2;
globdata.fint = f;
% globdata.fhatf1 = zeros(length(free),1);
% globdata.state = u_old; % initial guess
uhat = f;
% uhat(bnds.disp.top) = app_disp/2;
uhat(bnds.disp.bot) = app_disp;
globdata.uhatf = uhat(bnds.fixed);

fun = @(u,f)myfun(msh,u,f,model,Ke_all,Kindx,bnds,false);

% globdata.fint(bnds.disp.top) =  globdata.param1 *  app_disp/2;
% globdata.fint(bnds.disp.bot) =  globdata.param1 * -app_disp/2;

[~,~,Kt] = fun(globdata.state,globdata.fint);
% globdata.fint(free) = (Kt(free,free) * globdata.state(free)) + (Kt(free,bnds.fixed) * globdata.state(bnds.fixed)); % forces on free dofs
% fint = globdata.fint; % this remains constant
u_control = true; % problem is under displacement control
d = app_disp;
i = 1;
while (d < 40) % while the opening displacement is less than 10mm
    
%     globdata.state(bnds.disp.top) = globdata.param1 *  app_disp/2;
%     globdata.state(bnds.disp.bot) = globdata.param1 * -app_disp/2;
    [globdata,settings] = riksSolver2(msh,globdata,bnds,free,globdata.state,u_control,fun,settings);
    
    % post process
    if (settings.flag == 0)
        disp(i) = globdata.param1 * app_disp;
        d = disp(i);
        postprocess = true;
        u_new = globdata.state;
        bnds_free.fixed = [];
        [~,~,Kfree] = myfun(msh,u_new,globdata.fint,model,Ke_all,Kindx,bnds_free,postprocess);
        force = Kfree * u_new;
        F(i) = sum(abs(force));
        figure(1)
        hold on;
        grid on;
        box on;
        plot(disp(i),F(i),'vr');
        

        vector.name = 'U';
        vector.data = zeros(msh.nnode,3);

        vector.data(:,1) = u_new(1:msh.nnode);
        vector.data(:,2) = u_new(msh.nnode+1:2*msh.nnode);
        vector.data(:,3) = u_new(2*msh.nnode+1:3*msh.nnode);

        output_file = strcat('sol',num2str(i),'.vtk');

        output_title = strcat('sol.vtk ');

        matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)

        i = i + 1;

    end
end