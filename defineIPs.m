function [msh,coh] = defineIPs(msh)

    msh.nip = 8;
    
    P   =  1 / sqrt(3);
    
    msh.ip.coords(1,:) = [-P,-P,-P];
    msh.ip.coords(2,:) = [P,-P,-P];
    msh.ip.coords(3,:) = [P,P,-P];
    msh.ip.coords(4,:) = [-P,P,-P];
    
    msh.ip.coords(5,:) = [-P,-P,P];
    msh.ip.coords(6,:) = [P,-P,P];
    msh.ip.coords(7,:) = [P,P,P];
    msh.ip.coords(8,:) = [-P,P,P];

    msh.ip.wgts(1:8) = ones(msh.nip,1);
    
    % cohesive element ips 
    % coh.nip = 4;
    
    % coh.ip.coords(1,:) = [-P,-P,0];
    % coh.ip.coords(2,:) = [P,-P,0];
    % coh.ip.coords(3,:) = [P,P,0];
    % coh.ip.coords(4,:) = [-P,P,0];
    
    % coh.ip.wgts = ones(coh.nip,1);

% end
% function [msh,coh] = defineIPs(msh)

    % msh.nip = 27;
    
    % h = 1;
    % qpx = [-1:h:1];
    % wpx = [1/3 4/3 1/3];
    % qp = zeros(msh.nip,3);
    % wp = zeros(msh.nip,1);
    % cnt = 1;
    % for i = 1 : 3
    %     for j = 1 : 3
    %         for k = 1 : 3
    %             qp(cnt,:) = [qpx(k) qpx(j) qpx(i)];
    %             wp(cnt) = wpx(k) * wpx(j) * wpx(i);
    %             cnt = cnt + 1;
    %         end
    %     end
    % end
    
    % msh.ip.coords = qp;

    % msh.ip.wgts = wp;
    
    % cohesive element ips 
    %% Order 3
    coh.nip = 9;
    h = 1;
    qpx = [-1:h:1];
    wpx = [1/3 4/3 1/3];
    qp = zeros(coh.nip,3);
    wp = zeros(coh.nip,1);
    cnt = 1;
    for i = 1 : 3
        for j = 1 : 3
                qp(cnt,:) = [qpx(j) qpx(i) 0.5];
                wp(cnt) = wpx(j) * wpx(i);
                cnt = cnt + 1;
        end
    end
    
    coh.ip.coords = qp;
    coh.ip.wgts = wp;

    
    %% Order 4
    % coh.nip = 25;
    % qpx = linspace(-1,1,coh.nip^0.5);
    % h = 2/4;
    % wpx = [7 32 12 32 7] .* (2*h/45);
    % qp = zeros(coh.nip,3);
    % wp = zeros(coh.nip,1);
    % cnt = 1;
    % for i = 1 : 5
    %     for j = 1 : 5
    %             qp(cnt,:) = [qpx(j) qpx(i) 0.5];
    %             wp(cnt) = wpx(j) * wpx(i);
    %             cnt = cnt + 1;
    %     end
    % end
    
    % coh.ip.coords = qp;
    
    % coh.ip.wgts = wp;

    
    %% Order 10
    % coh.nip = 121;
    % qpx = linspace(-1,1,coh.nip^0.5);
    % h = 2/10;
    % wpx = [16067, 106300, -48525, 272400, -260550, 427368, -260550, 272400, -48525, 106300, 16067] .* (5.*h/299376);
    % qp = zeros(coh.nip,3);
    % wp = zeros(coh.nip,1);
    % cnt = 1;
    % for i = 1 : 11
    %     for j = 1 : 11
    %             qp(cnt,:) = [qpx(j) qpx(i) 0.5];
    %             wp(cnt) = wpx(j) * wpx(i);
    %             cnt = cnt + 1;
    %     end
    % end
    
    % coh.ip.coords = qp;
    
    % coh.ip.wgts = wp;
end
