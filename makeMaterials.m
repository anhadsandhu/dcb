function[isotropic,composite] = makeMaterials(model)
% Isotropic Resin
    lambda = model.E_R*model.nu_R/((1+model.nu_R)*(1-2*model.nu_R)); mu = model.E_R/(2*(1+model.nu_R));
    isotropic = zeros(6);
    isotropic(1:3,1:3) = lambda*ones(3);
    isotropic = isotropic + diag(mu*[2,2,2,1,1,1]);

    % Orthotropic Composite
    S = zeros(6);
    % S(1,1) = 1/model.E1; S(1,2) = -model.nu_21/model.E2; S(1,3) = -model.nu_31/model.E3;
    % S(2,1) = S(1,2);    S(2,2) = 1/model.E2; S(2,3) = -model.nu_32/model.E3;
    % S(3,1) = S(1,3);    S(3,2) = S(2,3);    S(3,3) = 1/model.E3;
    % S(4,4) = 1/model.G_23;
    % S(5,5) = 1/model.G_13;
    % S(6,6) = 1/model.G_12;
    
    model.nu_21 = model.nu_12 * model.E2 / model.E1;
    model.nu_31 = model.nu_13 * model.E2 / model.E1;
    model.nu_32 = model.nu_23 * model.E3 / model.E2;
    
    
    S(1,1) = 1/model.E1; S(1,2) = -model.nu_21/model.E2; S(1,3) = -model.nu_31/model.E3;
    S(2,1) = S(1,2);    S(2,2) = 1/model.E2; S(2,3) = -model.nu_32/model.E3;
    S(3,1) = S(1,3);    S(3,2) = S(2,3);    S(3,3) = 1/model.E3;
    S(4,4) = 1/model.G_23;
    S(5,5) = 1/model.G_13;
    S(6,6) = 1/model.G_12;

    composite = inv(S);


end