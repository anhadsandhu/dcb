function plotMesh(filename,msh)

    matlab2vtk(strcat(filename,'Coarse.vtk'),'Mesh', msh,'quad',[],[],[]);
    
    for j = 1 : msh.numDomain    
        
        
        xi.name = 'POU';

        xi.data = msh.Omg{j}.xi;
        
        
        matlab2vtk(strcat(filename,'_',int2str(j),'.vtk'),'Mesh',msh.Omg{j},'quad',xi,[],[]);       
    end
        
end