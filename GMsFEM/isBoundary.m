%% Apply boundary conditions
function [msh, bnds, bndid] = isBoundary(msh,varargin)
    
    L = max(msh.coords);
    
    %leftbnd = find(msh.coords(:,1) < 1e-6);
    topbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) > L(3)-1e-6);
    botbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) < 1e-6);
    
    tbnd = [topbnd; topbnd + msh.nnode; topbnd + msh.nnode * 2];
    bbnd = [botbnd; botbnd + msh.nnode; botbnd + msh.nnode * 2];

    disp_bnd = union(tbnd,bbnd); % dofs under displacement control
                                 % disp_bnd = union(lbnd,disp_bnd); % dofs under displacement control

    bnds.fixed = disp_bnd;%union(disp_bnd,msh.nnode+1:msh.nnode*2);
    bnds.disp.top = topbnd + msh.nnode*2;
    bnds.disp.bot = botbnd + msh.nnode*2;

    bnds.free = 1 : msh.tdof;
    bnds.free(bnds.fixed) = [];    
    
    % only need to do once for all subdomains
    if (nargin == 1)
        
        for i = 1 : msh.numDomain
            
            % For each subdomain
            
            [global_tbnd, local_tbnd, ~] = intersect(msh.Omg{i}.nodes,topbnd);
            [global_bbnd, local_bbnd, ~] = intersect(msh.Omg{i}.nodes,botbnd);

            
            tl = [local_tbnd; local_tbnd + msh.Omg{i}.nnode; local_tbnd + msh.Omg{i}.nnode * 2];
            bl = [local_bbnd; local_bbnd + msh.Omg{i}.nnode; local_bbnd + msh.Omg{i}.nnode * 2];
            
            msh.Omg{i}.bnds.fixed = union(tl, bl);
            
            msh.Omg{i}.bnds.free = 1 : 3*msh.Omg{i}.nnode;
            
            msh.Omg{i}.bnds.free(msh.Omg{i}.bnds.fixed) = [];
            
            tg = [global_tbnd; global_tbnd + msh.nnode; global_tbnd + msh.nnode * 2];
            
            bg = [global_bbnd; global_bbnd + msh.nnode; global_bbnd + msh.nnode * 2];
            
            msh.Omg{i}.bnds.fixed_global = union(tg, bg);
            
        end
        bndid = [];
        
        
        % if subdomain id is also provided, do this
    else
        id = varargin{1};
        % boundary in Omg{j} that are also global dirichlet
        bnd = intersect(bnds.fixed,msh.Omg{id}.fulldofb);
        % [~,iproc_bnd] = setdiff(msh.Omg{id}.fulldofb,msh.Omg{id}.fulldof);
        
        % lbnd = union(bnd,msh.Omg{id}.fulldofb(iproc_bnd)); % union of global bnds and local processor bnds
        [~,IA,IB] = intersect(msh.Omg{id}.fulldofb,bnd);%bnds.fixed);
        bndid = IA;%msh.Omg{id}.fulldofb(IB);
    end
    

end

