
clear all
% close all

addpath('functions/')
addpath('plotting/')
addpath('../')


% Define Coarse Discretisation in 2D


Nc = [3, 1, 1];

overlap = 3;




%% Define material properties
% isotropic
model.E_R = 0.5; %    GPa
model.nu_R = 0.4;

% composite
model.E1 = 150; %    GPa
model.E2 = 11; %    GPa
model.E3 = 11; %    GPa

model.nu_12 = 0.25; 
model.nu_13 = 0.25;
model.nu_23 = 0.45;

model.G_12 = 6;   %   GPa
model.G_13 = 6;   %   MPa
model.G_23 = 3.7; %   GPa

% cohesive
model.T = 45e-3;
model.S = 45e-3;
model.N = 45e-3;
model.GIc = 0.268e-3;
model.GIIc = 0.268e-3;
model.GIIIc = 0.268e-3;
model.K = 1e3;

model.alpha = 0; % power law criterion. Set this to 0 to use B-K criterion
model.eta = 2; % B-K criterion

%% Make material matrices
[isotropic,composite] = makeMaterials(model);
ss = [0,-1,0]; % -1 is interface
% ss = [0,0,-1,0,0]; % -1 is interface
layer_colours = [2.5,0,7.5];
% layer_colours = [1.5,2.5,0,7.5,8.5];

%% Meshing
msh = readMesh('dcb.msh','HEXAS');

msh.dim = 3;

matlab2vtk ("freshGrid.vtk","freshGrid", msh,'hex' , [], [], [])

msh = partitionMsh(msh,Nc, true); % true for plotting partition

msh = overlapDomains(msh, overlap);

msh = partitionOfUnity(msh,'cell');

plotPOU(msh)

msh.m = 1; % number of coarse modes per subdomain


[msh, bnds,~] = isBoundary(msh);

free = 1 : msh.tdof;
free(bnds.fixed) = [];

app_disp = 1e-3; % constant displacement boundary condition
max_disp = 16; % stopping criteria for crisfield solver

% Build FEM functions

% integrations points and shape functions
[msh,msh.coh] = defineIPs(msh);
[msh.N,msh.dN,msh.coh] = ShapeFunctions(msh,msh.coh);


%% Element stiffness matrix
indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

Ke_all = zeros(msh.nedof^2,msh.nelem);

regions.name = 'regions';
regions.data = zeros(msh.nelem,1);
numCohesive = 0; % counter for number of cohesive elements
msh.coh_elements = []; % store list of cohesive element ids
for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
        msh.elements{ie}.continuum = 1;
    else
        C = isotropic;
        ang = 0;
        msh.elements{ie}.continuum = 0;
        numCohesive = numCohesive + 1;
        msh.coh_elements = [msh.coh_elements; ie];
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    

    if(msh.elements{ie}.continuum == 1)
        Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    end
    
end

%% Define cracks
global Dmax;
Dmax = zeros(msh.coh.nip+2,msh.nelem); % +2 to store the state of the element whether damage is close to initiation and if the element is fully broken
for el = 1 : msh.nelem
    cen = mean(msh.coords(msh.elements{el}.connectivity));
    if (cen(1) >= 75 & ~msh.elements{el}.continuum)
        Dmax(:,el) = 1e6;
    end
end

% create set of cohesive elements contained in each subdomain
for ip = 1 : msh.numDomain
    msh.Omg{ip}.coh_elements = intersect(msh.Omg{ip}.elements, msh.coh_elements);
end

msh = subdomainDamage(msh);

KLold = sparse(Kindx.i',Kindx.j',Ke_all); % assemble only for continuum elements
[Ke_all,~,~,~] = myfun(msh,zeros(msh.tdof,1),zeros(msh.tdof,1),model,Ke_all,Kindx,bnds,free,false);

%% global data
globdata.state = zeros(msh.tdof,1);
globdata.F = zeros(msh.tdof,length(free));
for i = 1 : length(free)
    globdata.F(free(i),i) = 1;
end

f = zeros(msh.tdof,1);
uhat = f;
uhat(bnds.disp.top) = app_disp/2;
uhat(bnds.disp.bot) = -app_disp/2;
globdata.uhatf = uhat;%(bnds.fixed);

%% Calculate particular solution
% KLfull = V * KL * V';
global KLfull;
KLfull = sparse(Kindx.i',Kindx.j',Ke_all);
% uhat(bnds.disp.top) = 1e-6;
% uhat(bnds.disp.bot) = -1e-6;
ufree = KLfull(free,free) \ (f(free) - (KLfull(free,bnds.fixed) * uhat(bnds.fixed)));
psol = zeros(msh.tdof,1);
psol(free) = ufree;
psol(bnds.fixed) = uhat(bnds.fixed);
% psol = psol/sqrt(psol'*psol);
msh.psol = psol;

vector.name = 'U';
vector.data = zeros(msh.nnode,3);

vector.data(:,1) = psol(1:msh.nnode);
vector.data(:,2) = psol(msh.nnode+1:2*msh.nnode);
vector.data(:,3) = psol(2*msh.nnode+1:3*msh.nnode);

output_file = strcat('psol',num2str(0),'.vtk');

output_title = strcat('psol.vtk ');
regions.name = 'Regions';
regions.data = ones(msh.nelem,1);
matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)

[Ke_all,K,V,msh] = myfun(msh,zeros(msh.tdof,1),zeros(msh.tdof,1),model,Ke_all,Kindx,bnds,free,false);
% KL = 0.5 * (KL + KL');

% [~,~,~,~,~,V,~] = myfun(msh,zeros(msh.tdof,1),zeros(msh.tdof,1),model,Ke_all,Kindx,bnds,free,false,KLold,0);
%% function handle for model to be solved
pp = false;
fun = @(u,f,pp) myfun(msh,u,f,model,Ke_all,Kindx,bnds,free,pp); 

%% Solve and plot
e = load('dcb_exp_data.txt');
figure(100)
hold on;
grid on;
box on;
plot(e(:,1),e(:,2)./1000,'sr','MarkerFaceColor','r');

[u,lambda,iter,Aout,DeltaSout,D,Fr] = crisfield_arc_length(fun,globdata,msh,free,bnds,max_disp); %u,f,model,Ke_all,Kindx,pp,KL,K_c0,V

plot(e(:,1),e(:,2)./1000,'sr','MarkerFaceColor','r');


% Build Coarse Space
% 
% findBnd = @(dof) findGlobalBoundaries(dof, msh);
% 
% [coarseSpace, Kl, K] = buildCoarseSpace(msh, m, exp(perm),findBnd, GenEOEigenProblem);
% 
% f = ones(msh.nnode,1);
% 
% [KH, fH, RH] = assembleCoarseSpace(msh, coarseSpace, K,f);
% 
% 
% % Solve Coarse Problem
% 
% UH = KH \ fH;
% 
% uh = zeros(msh.nnode,1);
% 
% [~, ~, FREE] = findBnd(1:msh.nnode);
% 
% uh(FREE) = K(FREE,FREE) \ f(FREE);
% 
% plotSolution(RH * UH,msh, 'coarse', 'coarse_sol.vtk')
% 
% plotSolution(uh,msh, 'fine', 'fine_sol.vtk')

