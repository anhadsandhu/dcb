

function msh = buildFEMMesh(N, L,offset, isCoarse, NfineFactor)

 if (nargin < 3)
     offset = [0,0];
     isCoarse = true;
     NfineFactor = 10;
 end
 
 if(length(NfineFactor) < 2)
     
     NfineFactor = [NfineFactor, NfineFactor];
 end
 
    msh.dim = 2;


	msh.nnode = N(1) * N(2);
    
    msh.tdof = ((N(1) - 1) * NfineFactor(1) + 1) * ((N(2) - 1) * NfineFactor(2) + 1);

	msh.nelem = (N(1) - 1) * (N(2) - 1);

	msh.h(1) = L(1) / (N(1) - 1);
	msh.h(2) = L(2) / (N(2) - 1);

    if(isCoarse)
        msh.numDomain = msh.nnode;
        msh.Nc = N;
        msh.Nf = NfineFactor;
    end

	% Generate Coarse Mesh on [0, L(1)] x [0, L(2)]

	x = offset(1) + linspace(0, L(1), N(1));
	y = offset(2) + linspace(0, L(2), N(2));

	[xx, yy] = meshgrid(x,y);

	msh.coords(:,1) = xx(:);
	msh.coords(:,2) = yy(:);
	msh.coords(:,3) = zeros(msh.nnode,1);

	msh.elements = cell(msh.nelem, 1);
    
    cnt = 0;

	for i = 1 : N(1) - 1
		for j = 1 : N(2) - 1
			cnt = cnt + 1;
			msh.elements{cnt}.connectivity =[(i-1)*N(2)+j, i*N(2)+j, i*N(2)+j+1,(i-1)*N(2)+j+1];  %[(j-1)*N(1) + i,(j-1)*N(1) + i+1,j*N(1) + i + 1,j*N(1) + i];
			msh.elem(cnt,:) = msh.elements{cnt}.connectivity;
            
            
		end
    end
    
    if(isCoarse)
    
        msh.nn = cell(msh.numDomain,1);
        msh.type = zeros(msh.numDomain,1);

        msh.bb_min = cell(msh.numDomain,1);
        msh.bb_max = cell(msh.numDomain,1);
        
        msh.bb = cell(msh.numDomain,1);
        
        msh.quadrant = cell(msh.numDomain,1);
        msh.bb_nodes = cell(msh.numDomain,1);
        msh.Nfine = cell(msh.numDomain,1);

        msh.Omg = cell(msh.numDomain,1);
        
        L_ref(1) = msh.coords(N(2) + 1,1) - msh.coords(1,1);
        L_ref(2) = msh.coords(2,2) - msh.coords(1,2);

        for j = 1 : msh.numDomain

            edge = whichBoundary(msh.coords(j,:), offset, L);

            [msh.nn{j}, msh.type(j), msh.bb{j}, msh.quadrant{j}, msh.bb_nodes{j}] = findNearestNeighbours(j, msh, edge, N);

            msh.bb_min{j} = msh.coords(msh.nn{j}(1),:);
            
            msh.bb_max{j} = msh.coords(msh.nn{j}(end),:);
                    
            
            L_local = [msh.bb_max{j}(1) - msh.bb_min{j}(1), msh.bb_max{j}(2) - msh.bb_min{j}(2)];
            
            H = L_local ./ L_ref;
            
            Nfine = (NfineFactor .* H) + 1;
            
            msh.Nfine{j} = Nfine;
            
                   
            msh.Omg{j} = buildFEMMesh(Nfine, L_local,msh.bb_min{j}, false,1);
            
        end
        
        
        
        
             
    end
    
   
    
    
    
    
    
    
    


end