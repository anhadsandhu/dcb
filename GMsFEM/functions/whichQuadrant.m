function [id,node] = whichQuadrant(x,centreNode,quadrant,j)

	id = zeros(4,1);

	d = x - centreNode;

	nodeAll = [3,4,1,2];

	% Which Quatrant
    if(d(1) <= 0 && d(2) <= 0) % Quadrant 1
        id(1) = 1;
    end
    if(d(1)>= 0 && d(2) <= 0) % Quadrant 2
    	id(2) = 1;
    end
    if(d(1) >= 0 && d(2) >= 0) % Quadrant 3
    	id(3) = 1;
    end
    if(d(1) <= 0 && d(2) >= 0) % Quadrant 4
        id(4) = 1;
    end

    id_all = find(quadrant' .* id);
   

    id = id_all(1); % Take the first

    node = nodeAll(id);
end