
function msh = buildMesh2(L, N, Nc,plotPartition)

    msh.N = N;
    msh.nnode = N(1) * N(2);
    msh.nelem = (N(1) - 1) * (N(2) - 1);
    msh.h(1) = L(1)  / (N(1) - 1) ;
    msh.h(2) = L(2)  / (N(2) - 1) ;
    
    msh.numDomain = Nc(1) * Nc(2);
   

    % Generate Fine Quadilateral Mesh on [0,1]

    x1 = linspace(0,L(1),N(1));
    
    x2 = linspace(0,L(2),N(2));
    
    [xx,yy] = meshgrid(x1,x2);

    msh.coords = zeros(msh.nnode,3);

    msh.coords(:,1) = xx(:); 
    msh.coords(:,2) = yy(:);  
    msh.coords(:,3) = zeros(msh.nnode,1);

    msh.elements = cell(msh.nelem,1);
    cnt =  0;

    msh.midpoints = zeros(msh.nelem,3);

    for i  = 1 :  msh.N(1) - 1
        for j = 1 : msh.N(2) - 1
            cnt =  cnt +  1;
            % anti clockwise node numbering
            msh.elements{cnt}.connectivity = [(i-1)*msh.N(2) + j, i *msh.N(2) + j, i *msh.N(2) + j + 1, (i-1)*msh.N(2) + j + 1];
            msh.elem(cnt,:) = msh.elements{cnt}.connectivity;    
            msh.midpoints(cnt,:) = mean(msh.coords(msh.elements{cnt}.connectivity,:));
        end
    end
    
    % Build Partitioned Mesh
    

    msh.Omg = cell(msh.numDomain,1);

    part = zeros(msh.nelem,1);

    for ie = 1 : msh.nelem
        
        ind(1) = floor(Nc(1)* msh.midpoints(ie,1)/L(1))  +  1;
        
        ind(2) = floor(Nc(2)* msh.midpoints(ie,2)/L(2))  +  1;
        

        part(ie) = Nc(1) * (ind(2)-1) + ind(1);

    end

    for ip = 1 : msh.numDomain
        msh.Omg{ip}.elements = find(part == ip);
        msh.Omg{ip}.overlap = [];
    end
    
    if(plotPartition == true)
    
        partition.name = 'Partition_Data';

        partition.data = zeros(msh.nelem,1);

        for i = 1:msh.numDomain
            ie = msh.Omg{i}.elements;
            partition.data(ie) = i*ones(length(ie),1);
        end

        matlab2vtk('partition_test2D_initalpartition.vtk','Partion_Test2D Initial Partition', msh,'quad',[],[],partition);
        
    end

end