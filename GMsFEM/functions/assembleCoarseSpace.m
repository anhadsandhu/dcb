function [KH, fH, RH] = assembleCoarseSpace(msh, cS, K, f)


KH = sparse(cS{1}.totalSize,cS{1}.totalSize); % Could improve indexing here.

RH = sparse(msh.nnode, cS{1}.totalSize);

fH = zeros(cS{1}.totalSize,1);

for j = 1 : msh.numDomain
    
    id = cS{j}.globalIds;
    
    RH(:,id) = msh.Omg{j}.R' * cS{j}.Vtilde;
    
    %fH(id) = fH(id) + cS{j}.Vtilde' * f{j};
    
end

KH = RH' * K * RH;

fH = RH' * f;


end