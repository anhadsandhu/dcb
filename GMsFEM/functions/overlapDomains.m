function msh = overlapDomains(msh, overlap)

msh.sizeOverlap = overlap;

% Construct Overlapping Domain

if(msh.dim == 2)  
    nodel = 4;
else
    nodel = 8;
end

% Hack needed to get msh.elem

msh.elem = zeros(msh.nelem, nodel);

for ie = 1 : msh.nelem
    msh.elem(ie,:) = msh.elements{ie}.connectivity;
end

Ke = (1/nodel)*ones(nodel,1); Ke_all = repmat(Ke,1,msh.nelem);

indx_i = msh.elem';
indx_j = repmat(1:msh.nelem,nodel,1);

M  = sparse(indx_i,indx_j,Ke_all);

msh.nodeCnt = zeros(msh.nnode,1);

    for ip = 1 : msh.numDomain

        for i = 1 : msh.sizeOverlap

            e = zeros(msh.nelem,1); % Initial element marker to zero

            e(msh.Omg{ip}.elements) = ones(length(msh.Omg{ip}.elements),1); % Mark Element in current partition with 1

            tmp_elements = msh.Omg{ip}.elements;

            n = M * e; 

            msh.Omg{ip}.nodes = find(n > 0);

            if (i == msh.sizeOverlap)
                msh.Omg{ip}.dof = msh.Omg{ip}.nodes;

                %msh.Omg{ip}.R = sparse(1:length(msh.Omg{ip}.dof),msh.Omg{ip}.dof,ones(1,length(msh.Omg{ip}.dof)),length(msh.Omg{ip}.dof),msh.nnode);
            end

            n(msh.Omg{ip}.nodes) = ones(length(msh.Omg{ip}.nodes),1);

            e = M'*n;

            msh.Omg{ip}.elements = find(e > 0);


        end

        e = zeros(msh.nelem,1);

        e(msh.Omg{ip}.elements) = ones(length(msh.Omg{ip}.elements),1);

        n = M * e;

        msh.Omg{ip}.nodes = find(n > 0);

        msh.Omg{ip}.dofb = msh.Omg{ip}.nodes;

        msh.Omg{ip}.R = sparse(1:length(msh.Omg{ip}.dofb),msh.Omg{ip}.dofb,ones(1,length(msh.Omg{ip}.dofb)),length(msh.Omg{ip}.dofb),msh.nnode);

        msh.nodeCnt(msh.Omg{ip}.dof) =  msh.nodeCnt(msh.Omg{ip}.dof) +  ones(length(msh.nodeCnt(msh.Omg{ip}.dof)),1);

        msh.Omg{ip}.coords = msh.coords(msh.Omg{ip}.nodes,:);
        
        msh.Omg{ip}.nnode = length(msh.Omg{ip}.nodes);

    end
    
    
    
    for i = 1 :msh.numDomain
        
        msh.Omg{i}.overlap = [];
        
        for j = 1 : msh.numDomain
            
            if  (i ~= j)
                
                S = intersect(msh.Omg{i}.elements,msh.Omg{j}.elements);
                
            else
                
                S = [];
                
            end
            
            msh.Omg{i}.overlap = union(msh.Omg{i}.overlap,S);
            
        end
        
        msh.Omg{i}.elem = msh.elem(msh.Omg{i}.elements,:);
        msh.Omg{i}.e2g = msh.e2g(msh.Omg{i}.elements,:);
        msh.Omg{i}.nel = length(msh.Omg{i}.elements);
        msh.Omg{i}.nnode = length(msh.Omg{i}.nodes);
        
        msh.Omg{i}.lelem = zeros(length(msh.Omg{i}.elem(:,1)),8);
        counter = 0;
        for j  = 1 : length(msh.Omg{i}.nodes)
            
            counter =  counter  + 1;
            
            id =  find(msh.Omg{i}.elem == msh.Omg{i}.nodes(j));
            
            msh.Omg{i}.lelem(id) =  counter*ones(length(id),1);
            
            
        end
        
        
        
    end
    
  
end