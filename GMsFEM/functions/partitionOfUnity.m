function msh = partitionOfUnity(msh, type)

    if (nargin < 2)
        type = 'cell';
    end


    switch(lower(type))
        
      case('cell')
        
        xi_all = zeros(msh.nnode,1);

        for ip = 1 :  msh.numDomain

            xi_all(msh.Omg{ip}.dof) = xi_all(msh.Omg{ip}.dof) + 1;
        end

        xi_all = 1 ./ xi_all;

        for ip = 1 :  msh.numDomain

            msh.Omg{ip}.xi = xi_all(msh.Omg{ip}.dofb);

            msh.Omg{ip}.procbnd = setdiff(msh.Omg{ip}.dofb,msh.Omg{ip}.dof);
            [~,ia,ib] = intersect(msh.Omg{ip}.procbnd,msh.Omg{ip}.dofb);

            msh.Omg{ip}.xi(ib) = 0;

            msh.Omg{ip}.X = sparse(diag(msh.Omg{ip}.xi));

        end
        % test for POU
        for ip = 1 : msh.numDomain
            POU(:,ip) = msh.Omg{ip}.R' * msh.Omg{ip}.xi;
        end
        assert(all(sum(POU,2)) == 1);
        
      case('node')
        
        msh = nodeCentredPOU(msh);
        
    end


    
end



function msh = nodeCentredPOU(msh)

% Coordinates of Node

    dim = msh.dim;

    for j = 1 : msh.numDomain

        centreNode = msh.coords(j,1:dim);

        msh.Omg{j}.xi = zeros(msh.Omg{j}.nnode,1);

        for in = 1 : msh.Omg{j}.nnode
            
            [id,node] = whichQuadrant(msh.Omg{j}.coords(in,1:dim),centreNode,msh.quadrant{j},j);
            
            mp = mean([msh.bb{j}(id,1:dim); centreNode]);
            
            dx = abs(centreNode - msh.bb{j}(id,1:dim));
            
            xi = 2 * (msh.Omg{j}.coords(in,1:dim) - mp) ./ dx;
            
            msh.Omg{j}.xi(in) = phi(xi,node, dim);
            
        end
        
    end


end


function [val,sf] = phi(xi, node, dim)

    if (dim == 2)

        sf(1) = 0.25 * (1 - xi(1)) * (1 - xi(2));
        sf(2) = 0.25 * (1 + xi(1)) * (1 - xi(2));
        sf(3) = 0.25 * (1 + xi(1)) * (1 + xi(2));
        sf(4) = 0.25 * (1 - xi(1)) * (1 + xi(2));
        
    else
        assert(0 == 1, "Implementation currently only for 2D Partition of Unity")

    end

    val = sf(node);

end





