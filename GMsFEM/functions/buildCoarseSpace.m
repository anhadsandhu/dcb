

function [basis,Klocal, K] = buildCoarseSpace(msh, m, p, findBnd)


indx_j = repmat(1:4,4,1); indx_i = indx_j';

Klocal = cell(msh.numDomain,1);

basis = cell(msh.numDomain,1);


%%% Need to change to local FE calculation

totalSize = 0;

cnt = 1;


for j = 1 : msh.numDomain
    
    
    Kindx.i = msh.Omg{j}.lelem(:,indx_i(:));
    Kindx.j = msh.Omg{j}.lelem(:,indx_j(:));
    
    p_local = repmat(p(msh.Omg{j}.elements)',16,1);
    
    Ke_all = p_local.*repmat(msh.Ke(:),1,msh.Omg{j}.nel);
     
    Klocal{j} = sparse(Kindx.i',Kindx.j',Ke_all); % Neumann Matrix
    
    [NN,MM] = size(Klocal{j});
    
    Kindx.i = msh.Omg{j}.lelem(msh.Omg{j}.localOverlap,indx_i(:));
    Kindx.j = msh.Omg{j}.lelem(msh.Omg{j}.localOverlap,indx_j(:));
    
    Ko = sparse(Kindx.i',Kindx.j',Ke_all(:,msh.Omg{j}.localOverlap),NN,MM);
    
    Ko = msh.Omg{j}.X * Ko * msh.Omg{j}.X;
    
    [~, ~, FREE] = findGlobalBoundaries(msh.Omg{j}.dofb,msh);
    
    basis{j}.V = zeros(msh.Omg{j}.nnode,m);
    
    [basis{j}.V(FREE,:),D] = eigs(Klocal{j}(FREE,FREE),Ko(FREE,FREE),m,'SM');
    
    basis{j}.D = diag(D);
    
    basis{j}.numModes = length(basis{j}.D);
    
    id = cnt : (cnt + basis{j}.numModes - 1);
    
    basis{j}.globalIds = id;
    basis{j}.Vtilde = msh.Omg{j}.X * basis{j}.V;
    
    cnt = id(end) + 1;
   
end




for j = 1 : msh.numDomain
    basis{j}.totalSize = cnt - 1;
end


ptmp = repmat(p',16,1);

Ke_all = ptmp.*repmat(msh.Ke(:),1,msh.nelem);


indx_j = repmat(1:4,4,1); indx_i = indx_j';
Kindx.i = msh.elem(:,indx_i(:)); Kindx.j = msh.elem(:,indx_j(:));

K = sparse(Kindx.i',Kindx.j',Ke_all);



end