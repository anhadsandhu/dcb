

function msh = partitionMsh(msh, Nc, plotPartition)

    msh.numDomain = Nc(1) * Nc(2) * Nc(3);
    
    msh.L = zeros(3,1);
    
    for i = 1 : 3
        msh.L(i) = max(msh.coords(:,i)) - min(msh.coords(:,i));     
    end
 
    msh.Omg = cell(msh.numDomain,1);

    part = zeros(msh.nelem,1);
    
    msh.midpoints = zeros(msh.nelem, msh.dim);

    for ie = 1 : msh.nelem
        
        msh.midpoints(ie,:) = mean(msh.coords(msh.elements{ie}.connectivity,:));
        
        ind = zeros(3,1);
        
        for i  = 1 : 3
            ind(i) = floor(Nc(i)* msh.midpoints(ie,i)/msh.L(i))  +  1;
        end
     
        
        
        part(ie) = Nc(1) * Nc(2) * (ind(3) - 1) + Nc(1) * (ind(2)-1) + ind(1);

    end

    for ip = 1 : msh.numDomain
        msh.Omg{ip}.elements = find(part == ip);
        msh.Omg{ip}.overlap = [];
    end
    
    if(plotPartition == true)
    
        partition.name = 'Partition_Data';

        partition.data = zeros(msh.nelem,1);

        for i = 1:msh.numDomain
            ie = msh.Omg{i}.elements;
            partition.data(ie) = i*ones(length(ie),1);
        end

        matlab2vtk('partition_test2D_initalpartition.vtk','Partion_Test2D Initial Partition', msh,'hex',[],[],partition);
        
    end






end