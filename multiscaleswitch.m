
function [R,Rf,K,K_c,Ke_all,V,msh] = multiscaleswitch(msh,u,f,model,Ke_all,Kindx,bnds,free,postprocess,KL,K_c0,V)

% compute K(u)
    [K_c,KNL,Ke_all] = cohesiveElementStiffness(msh,u,model,postprocess,Ke_all,Kindx,K_c0);
    
    %% Compute local eigenvectors and build coarse space
    if (~exist('V'))
        indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
        V = [];
        cnt = 0;
        for ip = 1 : msh.numDomain % For each subdomain
            
            % get subdomain neighbours
            if (ip == 1) % first subdomain
                nb = ip + 1;
            elseif (ip == msh.numDomain) % last subdomain
                nb = ip - 1;
            else
                nb = [ip - 1, ip + 1];
            end
                
            msh.Omg{ip}.fulldofb = [msh.Omg{ip}.dofb; msh.Omg{ip}.dofb + msh.nnode; msh.Omg{ip}.dofb + 2 * msh.nnode];
            msh.Omg{ip}.fulldof = [msh.Omg{ip}.dof; msh.Omg{ip}.dof + msh.nnode; msh.Omg{ip}.dof + 2 * msh.nnode];
            
            % assemble local stiffness matrix
            elements = msh.Omg{ip}.elements;
            Kindx.i = msh.e2g(elements,indx_i(:));
            Kindx.j = msh.e2g(elements,indx_j(:));
            KL_local = sparse(Kindx.i',Kindx.j',Ke_all(:,elements));
            KL_local = 0.5 * (KL_local + KL_local');
            A = KL_local(msh.Omg{ip}.fulldofb,msh.Omg{ip}.fulldofb);         
            
            [~,~,id] = isBoundary(msh,ip);
            
            freeDof = 1:length(msh.Omg{ip}.fulldofb);
            
            freeDof(id) = []; % Remove boundary dof.
            
            R = blkdiag(msh.Omg{ip}.R,msh.Omg{ip}.R,msh.Omg{ip}.R);
            if (msh.Omg{ip}.damage_state < 0) % if subdomain is has no crack or is fully broken, construct reduced model
                
                m = msh.m; % number of modes per subdomain
                Vtilde = zeros(msh.Omg{ip}.nnode*3,m);
                [Vfree,Dfree] = eigs(A(freeDof,freeDof),m,'SM'); % standard eigenvalue problem
                Vtilde(freeDof,:) = Vfree;
                
                % add particular solution as the first mode
                if (isfield(msh,'psol'))
                    % Vtilde = [(R * msh.psol), Vtilde];
                end
                % Gram-Schmidt normalization of eigenvectors against particular solution. Particular solution itself is NOT normalized
                n = size(Vtilde,1);
                k = size(Vtilde,2);
                U = zeros(n,k);
                U(:,1) = Vtilde(:,1);%/sqrt(Vtilde(:,1)'*Vtilde(:,1));
                for i = 2:k
                    U(:,i) = Vtilde(:,i);
                    for j = 1:i-1
                        U(:,i) = U(:,i) - ( U(:,j)'*U(:,i) )/( U(:,j)'*U(:,j) )*U(:,j);
                    end
                    U(:,i) = U(:,i)/sqrt(U(:,i)'*U(:,i));
                end
                Vtilde = U(:,1:m); % drop all vectors after m
                
                % identify interface types between current subdomain (which is of type R) and its neighbours
                interface = strings(size(nb));
                for j = 1 : length(nb)
                    if (msh.Omg{nb(j)}.damage_state ~= 0)
                        interface(j) = 'RR'; % reduced-reduced interface
                    else
                        interface(j) = 'FR'; % full-reduced interface
                    end
                end
                
                if (all(strcmp(interface,'RR'))) % all interfaces are of type RR
                    X = blkdiag(msh.Omg{ip}.X,msh.Omg{ip}.X,msh.Omg{ip}.X); % standard partition of unity for RR interface
                else % NOTE an R subdomain cannot be sandwiched between two F type subdomains
                    ftype = find(strcmp(interface,'FR'));
                    ftype = nb(ftype); % identify which side neighbour is F type
                    [ovlp,ia,ib] = intersect(msh.Omg{ftype}.nodes,msh.Omg{ip}.nodes);
                    xi = msh.Omg{ip}.xi;
                    xi(ib) = 1; % set FR overlap to 1
                    X = diag(xi);
                    X = blkdiag(X,X,X); % one sided partition of unity
                end
                % assemble coarse space
                V = [V, R' * X * Vtilde]; 
            
            else % construct full model
                % identify interface types between current subdomain (which is of type F) and its neighbours
                interface = strings(size(nb));
                for j = 1 : length(nb)
                    if (msh.Omg{nb(j)}.damage_state >= 0)
                        interface(j) = 'FF'; % full-full interface
                    else
                        interface(j) = 'FR'; % full-reduced interface
                    end
                end
                
                if (all(strcmp(interface,'FF'))) % all interfaces are of type FF
                    if (length(nb) > 1) % if it is not a boundary subdomain
                        lnb = nb(1); % left hand neighbour
                        rnb = nb(2); % right hand neighbour
                        [ovlp,ia,ib] = intersect(msh.Omg{lnb}.nodes,msh.Omg{ip}.nodes);
                        Rinterior = setdiff(msh.Omg{ip}.nodes,ovlp); % each F type subdomain provides its interior and right side overlap. Left side overlap comes from ip - 1 subdomain
                        [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                        Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                    else % if it is a boundary subdomain
                        if (ip == msh.numDomain) % if it is the right boundary subdomain (left boundary remains as is)
                            [ovlp,ia,ib] = intersect(msh.Omg{nb}.nodes,msh.Omg{ip}.nodes);
                            Rinterior = setdiff(msh.Omg{ip}.nodes,ovlp); % each F type subdomain provides its interior and right side overlap. Left side overlap comes from ip - 1 subdomain
                            [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                            Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                        end
                    end
                else
                    if (length(nb) > 1) % if it is not a boundary subdomain
                        rtype = find(strcmp(interface,'FR'));
                        rtype = nb(rtype); % identify which side neighbour is R type
                        if (length(rtype) == 1) % if only one interface is FR
                            [ovlp,ia,ib] = intersect(msh.Omg{rtype}.nodes,msh.Omg{ip}.nodes);
                            Rinterior = setdiff(msh.Omg{ip}.nodes,ovlp); % each F type subdomain provides its interior and right side overlap. Left side overlap comes from ip - 1 subdomain
                            [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                            Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                        else % if both interfaces are FR
                            lnb = nb(1); % left hand neighbour
                            rnb = nb(2); % right hand neighbour
                            [lovlp,ia,ib] = intersect(msh.Omg{lnb}.nodes,msh.Omg{ip}.nodes);
                            [rovlp,ia,ib] = intersect(msh.Omg{rnb}.nodes,msh.Omg{ip}.nodes);
                            ovlp = union(lovlp,rovlp);
                            Rinterior = setdiff(msh.Omg{ip}.nodes,ovlp);
                            [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                            Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                        end
                        
                    else % if it is a boundary subdomain
                        rtype = find(strcmp(interface,'FR'));
                        rtype = nb(rtype); % identify which side neighbour is R type
                        [ovlp,ia,ib] = intersect(msh.Omg{rtype}.nodes,msh.Omg{ip}.nodes);
                        Rinterior = setdiff(msh.Omg{ip}.nodes,ovlp);
                        [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                        Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                    end
                end
                V = [V, R(Rinterior,:)'];
                % lowest processor rank contributes overlap
                % if cnt == 0 add the whole subdomain
                % if (cnt == 0)
                %     % xi = repmat(msh.Omg{ip}.xi,3,1);
                    
                %     [ovlp,ia,ib] = intersect(msh.Omg{ip-1}.nodes,msh.Omg{ip}.nodes);
                    
                %     subovlp = ovlp;% setdiff(ovlp,msh.Omg{ip-1}.procbnd); % overlap between F and R without the R processor boundary. That will be contributed by the F subdomain
                %     Rinterior = setdiff(msh.Omg{ip}.nodes,subovlp); % get ids of nodes shared between R and F subdomains
                %     [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                %     Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                    
                %     V = [V, R(Rinterior,:)'];
                    
                % else % else add only interior. Also exclude processor boundary of the previous subdomain
                %     xi = repmat(msh.Omg{ip}.xi,3,1);
                %     [procbnd,ia,ib] = intersect(msh.Omg{ip-1}.procbnd,msh.Omg{ip}.nodes);
                %     Rexterior1 = find(xi < 1);
                %     Rexterior2 = [ib; ib + msh.Omg{ip}.nnode; ib + 2*msh.Omg{ip}.nnode];
                %     Rexterior = union(Rexterior1,Rexterior2);
                %     Rinterior = 1 : length(xi);
                %     Rinterior(Rexterior) = [];
                    [~,~,ib] = intersect(msh.Omg{ip}.bnds.fixed,Rinterior);
                    msh.Omg{ip}.bnds.combined = ib;
                %     V = [V, R(Rinterior,:)'];
                % end
                % else add the whole thing
                % cnt = cnt + 1;
            end
            
        end
    end
    
    if (size(KL) == size(KNL))
        % K = V' * KL * V;%
        K = V' * (KL + KNL) * V; % sum of linear and nonlinear components of K
    else
        % K = KL;% 
        K = KL + (V' * KNL * V);
    end
    
    % K = V' * K * V;
    u = V' * u;

    R = (K * u);
    Rf = R;%(free);
    
end