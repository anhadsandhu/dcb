addpath('LINESEARCH/LINESEARCH/');
close all;
clear all;

% make materials
model.E_R = 0.5; %    GPa
model.nu_R = 0.4;

model.E1 = 122.7; %    GPa
model.E2 = 10.1; %    GPa
model.E3 = 10.1; %    GPa

model.nu_12 = 0.25; 
model.nu_13 = 0.25;
model.nu_23 = 0.45;

model.G_12 = 5.5;   %   GPa
model.G_13 = 5.5;   %   MPa
model.G_23 = 3.7; %   GPa

model.T = 100e-3;
model.S = 100e-3;
model.N = 80e-3;
model.GIc = 0.969e-3;
model.GIIc = 1.719e-3;
model.GIIIc = 1.719e-3;
model.K = 1e3;

crackLen = 39.3;

[isotropic,composite] = makeMaterials(model);
ss = [0,-1,0]; % -1 is interface
layer_colours = [2.5,0,7.5];
% read mesh
msh = readMesh('enf.msh','HEXAS');
L = max(msh.coords);

% integrations points and shape functions
[msh,msh.coh] = defineIPs(msh);
[msh.N,msh.dN,msh.coh] = ShapeFunctions(msh,msh.coh);
% element stiffness matrix

indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

Ke_all = zeros(msh.nedof^2,msh.nelem);

regions.name = 'regions';
regions.data = zeros(msh.nelem,1);
numCohesive = 0; % counter for number of cohesive elements
for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
        msh.elements{ie}.continuum = 1;
    else
        C = isotropic;
        ang = 0;
        msh.elements{ie}.continuum = 0;
        numCohesive = numCohesive + 1;
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    
    if(msh.elements{ie}.continuum == 1)
        Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    end
    
end
global Dmax;
Dmax = zeros(msh.coh.nip,msh.nelem);
for el = 1 : msh.nelem
    cen = mean(msh.coords(msh.elements{el}.connectivity));
    if (cen(1) >= (L(1) - crackLen) & ~msh.elements{el}.continuum)
        Dmax(:,el) = 10;
    end
    
end
% nonlinear
% apply BCs
leftbnd = find(msh.coords(:,1) < 1e-6);
botbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) < 1e-6);
lbnd = [leftbnd; leftbnd + msh.nnode; leftbnd + msh.nnode * 2];
bbnd = [botbnd + msh.nnode * 2];

disp_bnd = union(lbnd,bbnd); % dofs under displacement control
bnds.fixed = disp_bnd;
bnds.disp.bot = botbnd + msh.nnode*2;

fun = @(u,f)myfun(msh,u,f,model,Ke_all,Kindx,bnds,false);

% newton solver (without line search)
app_disp = 1e-3;
epsilon = 1e-6;
u_old = zeros(msh.tdof,1);
u_conv = u_old;
i = 1;
conv = false;
while (app_disp < 50)
    if (i > 1)
        app_disp = app_disp + (0.1 * sqrt(10/cnt));
    end
    f = zeros(msh.tdof,1);
    % f(bnds.disp.top) = app_disp/2;
    f(bnds.disp.bot) = app_disp;

    conv = false;
    cnt = 0;
    while (true)
        % u_old(bnds.disp.top) = app_disp/2;
        u_old(bnds.disp.bot) = app_disp;
        [res,K] = fun(u_old,f);
        % norm(res)
        if (norm(res) < epsilon)
            conv = true;
            break;
        end
        u_new = u_old - (K \ res);
        u_old = u_new;
        cnt = cnt + 1;
    end
    
    % post process
    if (conv)
        postprocess = true;
        bnds_free.fixed = [];
        [~,Kfree] = myfun(msh,u_new,f,model,Ke_all,Kindx,bnds_free,postprocess);
        force = Kfree * u_new;
        F(i) = sum(abs(force(bnds.disp.bot)));
        figure(1)
        hold on;
        plot(app_disp,F(i),'or');
        
        [i app_disp F(i) cnt]
        
        disp(i) = app_disp;

        u = u_new;
        vector.name = 'U';
        vector.data = zeros(msh.nnode,3);

        vector.data(:,1) = u(1:msh.nnode);
        vector.data(:,2) = u(msh.nnode+1:2*msh.nnode);
        vector.data(:,3) = u(2*msh.nnode+1:3*msh.nnode);

        output_file = strcat('sol',num2str(i),'.vtk');

        output_title = strcat('sol.vtk ');

        matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)
        i = i + 1;
    end
end
