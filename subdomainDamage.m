function msh = subdomainDamage(msh)
    
% This function takes an input mesh and outputs the damage state variable of each subdomain. It computes the following
% 1. damage_begun = 1 if any integration point in the subdomain exceeds 80% of its critical relative displacement
% 2. damage_complete = 1 iff all the integration points in a subdomain have failed completely.
    
% Any subdomain can have three possible damage states
% a. -1 -> none of the integration points is close to damage onset
% b. 0 -> threshold (80%) for damage onset exceeded at at least one integration point in the subdomain
% c. 1 -> Damage value = 1 at all integration points in the subdomain i.e. all cohesive elements in the subdomain are fully broken.
    
    global Dmax; % access global Dmax matrix
    
    % for all subdomains
    for ip = 1 : msh.numDomain
        
        msh.Omg{ip}.damage_state = -1; % linear part of cohesive law
        
        % for all cohesive elements in subdomain
        
        damage_begun = any(Dmax(end-1,msh.Omg{ip}.coh_elements));
        
        damage_complete = all(Dmax(end,msh.Omg{ip}.coh_elements));
        
        if damage_begun == 1
            msh.Omg{ip}.damage_state = 0;
        end
        if damage_complete == 1
            msh.Omg{ip}.damage_state = 1;
        end
        
        %% temporary
        % msh.Omg{ip}.damage_state = -1;
        % if (ip > 1)
        %     msh.Omg{ip}.damage_state = 1;
        % end
    end
end