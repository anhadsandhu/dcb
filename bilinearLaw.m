function [Dsr,scalar,loading] = bilinearLaw(Delta,model,postprocess,ip,el)

    Do = [model.T/model.K, model.S/model.K, model.N/model.K];
    Df = [2*model.GIIIc/model.T, 2*model.GIIc/model.S, 2*model.GIc/model.N];
    assert(all(Df >= Do), "Material properties chosen unwisely.");
    
    Dshear = sqrt(Delta(1)^2 + Delta(2)^2);
    if(Delta(3) > 0)
        beta = Dshear / Delta(3);
    else
        beta =  Dshear;
    end

    
    D3 = Delta(3);
    if(D3 <= 0);
        D3 = 0;
    end
    
    Dm = sqrt(Dshear^2 + D3^2); % mixed mode relative displacement
    
    % softening onset
    if (Delta(3) > 0)
        Dmo = Do(3)*Do(1) * sqrt((1 + beta^2) / (Do(1)^2 + (Do(3)^2*beta^2)));
    else
        Dmo = sqrt(Do(1)^2 + Do(2)^2);
    end

    if (model.alpha ~= 0)
        % total decohesion - Power law criterion
        alpha = model.alpha;
        if (Delta(3) > 0)
            br1 = (1 / model.GIc)^alpha;
            br2 = (beta^2 / model.GIIc)^alpha;
            Dmf = (2*(1+beta^2)/(model.K*Dmo)) * (br1 + br2)^(-1/alpha);
        else
            Dmf = sqrt(Df(1)^2 + Df(2)^2);
        end
    else
        % total decohesion - B-K criterion
        eta = model.eta;
        if (Delta(3) > 0)
            br = model.GIc + ((model.GIIc - model.GIc) * (beta^2/(1 + beta^2))^eta);
            Dmf = (2 * br) / (model.K * Dmo);
        else
            Dmf = sqrt(Df(1)^2 + Df(2)^2);
        end
    end
    
    assert(all(Dmf >= Dmo), "Check material properties.");
    
    global Dmax;
    dmax = Dmax(ip,el);
    dmax = max(Dm,dmax);
    
    d = (Dmf * (dmax - Dmo)) / (dmax * (Dmf - Dmo));
    if(d > 1)
        d = 1;
    end
    if(d < 0)
        d = 0;
    end
    
    minusD3 = -Delta(3);
    if(minusD3 <= 0)
        minusD3 = 0;
    end
    
    % compute constitutive law
    Dsr = zeros(3);
    
    loading = 0;
    
    if(dmax <= Dmo) % no damage
        Dsr = eye(3) * model.K;
        loading = 0;
    elseif (dmax > Dmo & dmax < Dmf) % softening onset
        Dsr = (1 - d) * model.K * eye(3);
        Dsr(3,3) = Dsr(3,3) + (model.K * d * minusD3 / -Delta(3));
        if(Delta(3) == 0)
            Dsr(3,3) = (1 - d) * model.K;
        end
        loading = 1;
        if(Dm <= dmax) % if no damage growth
            loading = 0;
        end
    else % total decohesion
        Dsr(3,3) = model.K * minusD3 / -Delta(3);
        if(Delta(3) == 0)
            Dsr(3,3) = 0;
        end
        loading = 0;
    end
    if (Dm < dmax)
        loading = 0;
    end
        
    scalar = model.K * Dmf * Dmo / (Dm * dmax^2 * (Dmf - Dmo));
    
    if(postprocess)
        Dmax(ip,el) = dmax;
        num_ip = length(Dmax(:,el)) - 2; % because last two store the state of the element
        if(ip == num_ip) 
            % if damage at any integration point > 0, Dmax(end-1,el) = 1 i.e. damage is about to begin in that element
            Dmax(end-1,el) = any(Dmax(1:num_ip,el) > 0.8 * Dmo);
            % if damage at all integration points > Dmf, Dmax(end,el) = 1 i.e. element is fully broken
            Dmax(end,el) = all(Dmax(1:num_ip,el) > Dmf);
        end
    end
end

