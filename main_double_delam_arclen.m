% close all;
clear all;

%% Define material properties

% isotropic
model.E_R = 0.5; %    GPa
model.nu_R = 0.4;

% composite
model.E1 = 115; %    GPa
model.E2 = 8.5; %    GPa
model.E3 = 8.5; %    GPa

model.nu_12 = 0.29; 
model.nu_13 = 0.29;
model.nu_23 = 0.3;

model.G_12 = 4.5;   %   GPa
model.G_13 = 4.5;   %   MPa
model.G_23 = 3.2; %   GPa

% cohesive
model.T = 7.0e-3;
model.S = 7.0e-3;
model.N = 3.3e-3;
model.GIc = 0.33e-3;
model.GIIc = 0.8e-3;
model.GIIIc = 0.8e-3;
model.K = 1e2;

model.alpha = 0; % power law criterion; if zero, code used B-K criterion
model.eta = 2; % B-K criterion

%% Make material matrices
[isotropic,composite] = makeMaterials(model);
ss = [0,-1,0,-1,0]; % -1 is interface
% ss = [0,0,-1,0,0,-1,0,0]; % -1 is interface
layer_colours = [2.5,2.5,0,7.5,7.5];
% layer_colours = [2.5,2.5,2.5,0,0,7.5,7.5,7.5];% read mesh

%% Meshing
msh = readMesh('cze.msh','HEXAS');

% integrations points and shape functions
[msh,msh.coh] = defineIPs(msh);
[msh.N,msh.dN,msh.coh] = ShapeFunctions(msh,msh.coh);

%% Element stiffness matrix
regions.name = 'regions';
regions.data = zeros(msh.nelem,1);
numCohesive = 0; % counter for number of cohesive elements
for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
        msh.elements{ie}.continuum = 1;
    else
        C = isotropic;
        ang = 0;
        msh.elements{ie}.continuum = 0;
        numCohesive = numCohesive + 1;
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    
    if(msh.elements{ie}.continuum == 1)
        Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    end
    
end

%% Define cracks
global Dmax;
% global Dmax1;
% global Dmax2;
% global Dmax3;
% Dmax1 = zeros(msh.coh.nip,msh.nelem);
% Dmax2 = Dmax1;
% Dmax3 = Dmax1;
Dmax = zeros(msh.coh.nip+2,msh.nelem); % +2 to store the state of the element whether damage is close to initiation and if the element is fully broken
for el = 1 : msh.nelem
    cen = mean(msh.coords(msh.elements{el}.connectivity,:));
    if (cen(3) < 1.59)
        if (cen(1) >= 20 & cen(1) <= 40 & ~msh.elements{el}.continuum)
            Dmax(:,el) = 1e6;
            % Dmax2(:,el) = 1e6;
            % Dmax3(:,el) = 1e6;
        end
    else
        if (cen(1) >= 60 & ~msh.elements{el}.continuum)
            Dmax(:,el) = 1e6;
            % Dmax2(:,el) = 1e6;
            % Dmax3(:,el) = 1e6;
        end
    end
    
end

%% Apply boundary conditions
L = max(msh.coords);
leftbnd = find(msh.coords(:,1) < 1e-6);
topbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) > L(3)-1e-6);
botbnd = find(msh.coords(:,1) >  L(1)-1e-6 & msh.coords(:,3) < 1e-6);
% lbnd = [leftbnd; leftbnd + msh.nnode; leftbnd + msh.nnode * 2];
% tbnd = [topbnd + msh.nnode * 2];
% bbnd = [botbnd + msh.nnode * 2];

tbnd = [topbnd; topbnd + msh.nnode; topbnd + msh.nnode * 2];
bbnd = [botbnd; botbnd + msh.nnode; botbnd + msh.nnode * 2];

disp_bnd = union(tbnd,bbnd); % dofs under displacement control
% disp_bnd = union(lbnd,disp_bnd); % dofs under displacement control

% ybnd = [1:msh.nnode] + msh.nnode;
% disp_bnd = union(disp_bnd,ybnd);

bnds.fixed = disp_bnd;%union(disp_bnd,msh.nnode+1:msh.nnode*2);
bnds.disp.top = topbnd + msh.nnode*2;
bnds.disp.bot = botbnd + msh.nnode*2;

free = 1 : msh.tdof;
free(bnds.fixed) = [];

app_disp = 1e-2; % constant displacement boundary condition.
max_disp = 21; % displacement till which solver will run

%% global data
globdata.state = zeros(msh.tdof,1);
globdata.F = zeros(msh.tdof,length(free));
for i = 1 : length(free)
    globdata.F(free(i),i) = 1;
end

f = zeros(msh.tdof,1);
uhat = f;
uhat(bnds.disp.top) = app_disp/2;
uhat(bnds.disp.bot) = -app_disp/2;
globdata.uhatf = uhat(bnds.fixed);

%% Assemble K
indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

KL = sparse(Kindx.i',Kindx.j',Ke_all); % assemble only for continuum elements
[~,~,KL,K_c0] = myfun(msh,zeros(msh.tdof,1),f,model,Ke_all,Kindx,bnds,free,false,KL,0);

%% function handle for model to be solved
pp = false;
fun = @(u,f,pp)myfun(msh,u,f,model,Ke_all,Kindx,bnds,free,pp,KL,K_c0);

%% Solve and plot
e = load('double_delam_exp_data.txt');
figure(100)
hold on;
grid on;
box on;
plot(e(:,1),e(:,2)./1000,'sr','MarkerFaceColor','r');

[u,lambda,iter,Aout,DeltaSout,D,Fr] = crisfield_arc_length(fun,globdata,msh,free,bnds,max_disp);

plot(e(:,1),e(:,2)./1000,'sr','MarkerFaceColor','r');
    
    % D = mean(abs(u(bnds.disp.bot,:))) .* 2;
    % for k = 1 : size(u,2)
    %     disp(k) = D(k);
    %     d = disp(k);
    %     postprocess = true;
    %     u_new = (u(:,k));
    %     bnds_free.fixed = [];
    %     [~,~,Kfree] = myfun(msh,u_new,globdata.fint,model,Ke_all,Kindx,bnds_free,postprocess);
    %     force = Kfree * u_new;
    %     F(k) = sum(abs(force));
    %     figure(1)
    %     hold on;
    %     grid on;
    %     box on;
    %     plot(disp(k),F(k),'vr');
        
    %     vector.name = 'U';
    %     vector.data = zeros(msh.nnode,3);

    %     vector.data(:,1) = u_new(1:msh.nnode);
    %     vector.data(:,2) = u_new(msh.nnode+1:2*msh.nnode);
    %     vector.data(:,3) = u_new(2*msh.nnode+1:3*msh.nnode);

    %     output_file = strcat('sol',num2str(k),'.vtk');

    %     output_title = strcat('sol.vtk ');

    %     matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)
    % end
    
    
%     % post process
%     if (settings.flag == 0)
%         disp(i) = globdata.param1 * app_disp;
%         d = disp(i);
%         postprocess = true;
%         u_new = globdata.state;
%         bnds_free.fixed = [];
%         [~,~,Kfree] = myfun(msh,u_new,globdata.fint,model,Ke_all,Kindx,bnds_free,postprocess);
%         force = Kfree * u_new;
%         F(i) = sum(abs(force));
%         figure(1)
%         hold on;
%         grid on;
%         box on;
%         plot(disp(i),F(i),'ok');
        

%         vector.name = 'U';
%         vector.data = zeros(msh.nnode,3);

%         vector.data(:,1) = u_new(1:msh.nnode);
%         vector.data(:,2) = u_new(msh.nnode+1:2*msh.nnode);
%         vector.data(:,3) = u_new(2*msh.nnode+1:3*msh.nnode);

%         output_file = strcat('sol',num2str(i),'.vtk');

%         output_title = strcat('sol.vtk ');

%         matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)

%         i = i + 1;

%     end
% end