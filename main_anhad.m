%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        Model Setup 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numEigs = 20;

filename = 'layercake.msh';
numLayers = 7;
numParts = 5; % number of sub domains
sizeOverlap = 2; % in number of layers
ss = [0,-1,pi/2,-1,0,-1,pi/4]; % -1 is interface

layer_colours = [2.5,0,7.5,0,2.5,0,5];

E_R = 0.5; %    GPa
nu_R = 0.4;

E1 = 135; %    GPa
E2 = 0.5; %    GPa
E3 = 0.5; %    GPa

nu_21 = 0.022; 
nu_31 = 0.022;
nu_32 = 0.4;

G_12 = 0.01;   %   GPa
G_13 = 0.01;   %   MPa
G_23 = 0.01; %   GPa

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[isotropic,composite] = makeMaterials(E_R,nu_R,E1,E2,E3,nu_21,nu_31,nu_32,G_12,G_13,G_23);

msh = readMesh(filename,'HEXAS');

msh = createParts(numParts, sizeOverlap, msh);

msh = defineIPs(msh);

[msh.N,msh.dN] = ShapeFunctions(msh);

%%%%%% Construct element stiffness matrix

% For Each Element

indx_j = repmat(1:24,24,1); indx_i = indx_j';
Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));

Ke_all = zeros(24^2,msh.nelem);

regions.name = 'regions';
regions.data = zeros(msh.nelem,1);

for ie = 1:msh.nelem
    
    if ss(msh.elements{ie}.region) >= 0
        C = composite;
        ang = ss(msh.elements{ie}.region);
    else
        C = isotropic;
        ang = 0;
    end
    
    regions.data(ie) = layer_colours(msh.elements{ie}.region);
    
    Ke_all(:,ie) = elementStiffness(msh.coords(msh.elements{ie}.connectivity,:),msh.elements{ie},msh.dN,msh.nip,C,ang,msh.ip); 
    
end

K = sparse(Kindx.i',Kindx.j',Ke_all);

[V,D] = eigs(K,numEigs,'SM');

for i = 1 : numEigs
    
    vector.name = 'displacements';
    vector.data = zeros(msh.nnode,3);
    
    vector.data(:,1) = V(1:msh.nnode,i);
    vector.data(:,2) = V(msh.nnode+1:2*msh.nnode,i);
    vector.data(:,3) = V(2*msh.nnode+1:3*msh.nnode,i);
    
    output_file = strcat('eigenvector',num2str(i),'.vtk');
    
    output_title = strcat('eigenvector ',num2str(i),' eigenvalue = ',num2str(D(i,i)));

    matlab2vtk (output_file,output_title, msh,'hex' , [], vector, regions)

end
