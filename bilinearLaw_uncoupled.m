function [Dsr,scalar,loading] = bilinearLaw_uncoupled(Delta,model,postprocess,ip,el)

    Do = [model.T/model.K, model.S/model.K, model.N/model.K];
    Df = [2*model.GIIIc/model.T, 2*model.GIIc/model.S, 2*model.GIc/model.N];
    assert(all(Df >= Do), "Material properties chosen unwisely.");
    
    global Dmax1;
    global Dmax2;
    global Dmax3;
    dmax = zeros(3,1);
    dmax(1) = Dmax1(ip,el);
    dmax(2) = Dmax2(ip,el);
    dmax(3) = Dmax3(ip,el);
    
    for i = 1 : 3
        dmax(i) = max(Delta(i),dmax(i));
    end
    
    d = zeros(3,1);
    for i = 1 : 3
        d(i) = (Df(i) * (dmax(i) - Do(i))) / (dmax(i) * (Df(i) - Do(i)));

        if(d(i) > 1)
            d(i) = 1;
        end
        if(d(i) < 0)
            d(i) = 0;
        end
    end
    
    minusD3 = -Delta(3);
    if(minusD3 <= 0)
        minusD3 = 0;
    end
    
    % compute constitutive law
    Dsr = zeros(3);
    
    loading = zeros(3,1);
    
    for i = 1 : 3
        if(dmax(i) <= Do(i)) % no damage
            Dsr(i,i) = model.K;
        elseif (dmax(i) > Do(i) & dmax(i) < Df(i)) % softening onset
            Dsr(i,i) = (1 - d(i)) * model.K;
            if(i == 3)
                Dsr(3,3) = Dsr(3,3) + (model.K * d(3) * minusD3 / -Delta(3));
                if(Delta(3) == 0)
                    Dsr(3,3) = (1 - d(i)) * model.K;
                end
            end
            loading(i) = 1;
            if(Delta(i) <= dmax(i)) % if no damage growth
                loading(i) = 0;
            end
        else % total decohesion
            if (i == 3)
                Dsr(3,3) = model.K * minusD3 / -Delta(3);
                if(Delta(3) == 0)
                    Dsr(3,3) = 0;
                end
            end
        end
        if (Delta(i) < dmax(i))
            loading(i) = 0;
        end
    end
    
    % if(abs(dmax(1)) >= Do(1) | abs(dmax(2)) >= Do(2))
    %     dmax
    % end
        
    for i = 1 : 3
        scalar(i) = model.K * Df(i) * Do(i) / (Delta(i) * dmax(i)^2 * (Df(i) - Do(i)));
    end

    if(postprocess)
        Dmax1(ip,el) = dmax(1);
        Dmax2(ip,el) = dmax(2);
        Dmax3(ip,el) = dmax(3);
    end
end

