function [u_new,it,conv] = newton_raphson(u,f,bnds,free,app_disp,funhandle,epsilon)
% plain vanilla Newton-Rahpson nonlinear solver
    conv = false;
    it = 0;
    u_old = u;
    
    while(true)
        if (it == 0)
            u_old(bnds.disp.top) = app_disp/2;
            u_old(bnds.disp.bot) = -app_disp/2;
            [res,~,K] = funhandle(u_old,f);
            % [res,K] = myfun(msh,u_old,f,model,Ke_all,Kindx,bnds,false);
            norm(res)
            if (norm(res) < epsilon)
                u_new = u_old;
                conv = true;
                break;
            end
            
            u_new = u_old - (K \ res);
            u_old = u_new;
            it = it + 1;
            
        end
        
    end
end

