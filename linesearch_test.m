close all;
clear all;

x = 1;%0.865474033102;

fun = @(x)testfun(x);
[f,k] = fun(x);
epsilon = 1e-14;
x_old = x;
qoi = f;%x_old;
cnt = 0;
while (any(qoi) > epsilon)
    [res,K] = fun(x_old);
    if (abs(res) < epsilon)
        break;
    end
    x_new = x_old - (res/K);
    if(abs(x_old - x_new) < epsilon)
        break;
    end
    % x_new = x_old + (D);
    % [res_new,~] = fun(x_new);
    % qoi = abs(res_new ./ res);
    x_old = x_new
    % if (norm(D) < epsilon)
    %     break;
    % end
    % norm(x_old - x_new)
    cnt = cnt + 1;
end
% [xk,k] = linesearch(fun,f,0.5,4)
