function [Ke_all,K,V,msh] = myfun(msh,u,f,model,Ke_all,Kindx,bnds,free,postprocess)

% compute K(u)
    Ke_all = cohesiveElementStiffness(msh,u,model,postprocess,Ke_all);

    %% Compute local eigenvectors and build coarse space
    if true%(~exist('V'))
        indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
        V = [];
        cnt = 0;
        for ip = 1 : msh.numDomain % For each subdomain
            
            msh.Omg{ip}.fulldofb = [msh.Omg{ip}.dofb; msh.Omg{ip}.dofb + msh.nnode; msh.Omg{ip}.dofb + 2 * msh.nnode];
            msh.Omg{ip}.fulldof = [msh.Omg{ip}.dof; msh.Omg{ip}.dof + msh.nnode; msh.Omg{ip}.dof + 2 * msh.nnode];
            
            % assemble local stiffness matrix
            elements = msh.Omg{ip}.elements;
            Kindx.i = msh.e2g(elements,indx_i(:));
            Kindx.j = msh.e2g(elements,indx_j(:));
            KL_local = sparse(Kindx.i',Kindx.j',Ke_all(:,elements));
            KL_local = 0.5 * (KL_local + KL_local');
            A = KL_local(msh.Omg{ip}.fulldofb,msh.Omg{ip}.fulldofb);
            
            % assemble overlap matrix
            overlap = msh.Omg{ip}.overlap;
            Kindx.i = msh.e2g(overlap,indx_i(:));
            Kindx.j = msh.e2g(overlap,indx_j(:));
            [NN,MM] = size(KL_local);
            KL_overlap = sparse(Kindx.i',Kindx.j',Ke_all(:,overlap),NN,MM);
            KL_overlap = 0.5 * (KL_overlap + KL_overlap');
            Ao = KL_overlap(msh.Omg{ip}.fulldofb,msh.Omg{ip}.fulldofb);
            
            [~,~,id] = isBoundary(msh,ip);
            
            freeDof = 1:length(msh.Omg{ip}.fulldofb);
            % [~,pbid,~] = intersect(msh.Omg{ip}.dofb,msh.Omg{ip}.procbnd);
            % procbnd = [pbid;pbid+msh.Omg{ip}.nnode;pbid+2*msh.Omg{ip}.nnode];
            freeDof(id) = []; % Remove boundary dof.
            % freeDof = setdiff(freeDof,procbnd);
            
            % partition of unity
            X = blkdiag(msh.Omg{ip}.X,msh.Omg{ip}.X,msh.Omg{ip}.X);
            Ao = X * Ao * X;
            if (msh.Omg{ip}.damage_state ~= 0) % if subdomain contains no crack
                
                m = msh.m; 
                if (ip > 2)
                    m = 1;
                end
                Vtilde = zeros(msh.Omg{ip}.nnode*3,m);
                eigopts.tol = 1e-8;
                % eigopts.p = 2*m;
                % eigopts.fail = 'replacenan';
                [Vfree,Dfree] = eigs(A(freeDof,freeDof),Ao(freeDof,freeDof),m,'SM',eigopts); % local generalised eigenvalue problem
                % [Vfree,Dfree] = eigs(A(freeDof,freeDof),m,'SM');
                % assemble coarse space    
                Vtilde(freeDof,:) = Vfree;
                % Vtilde = X * Vtilde;
                
                R = blkdiag(msh.Omg{ip}.R,msh.Omg{ip}.R,msh.Omg{ip}.R);
                if (isfield(msh,'psol'))
                    % fprintf('adding psol\n');
                    psol_local = R * msh.psol;
                    Vtilde = [psol_local, Vtilde];
                    % Vtilde(id,1) = 0;
                end
                
                % Gram-Schmidt normalization of eigenvectors against particular solution. Particular solution itself is ALSO normalized
                n = size(Vtilde,1);
                k = size(Vtilde,2);
                U = zeros(n,k);
                % U = Vtilde;
                U(:,1) = Vtilde(:,1);%/sqrt(Vtilde(:,1)'*Vtilde(:,1));
                for i = 2:k
                    U(:,i) = Vtilde(:,i);
                    for j = 1:i-1
                        U(:,i) = U(:,i) - ( U(:,j)'*U(:,i) )/( U(:,j)'*U(:,j) )*U(:,j);
                    end
                    U(:,i) = U(:,i)/sqrt(U(:,i)'*U(:,i));
                end
                % U = Vtilde;
                Vtilde = U(:,1:m);
                % if (isfield(msh,'psol'))
                %     Vtilde(id,2:end) = repmat(Vtilde(id,1),1,m-1);
                % end
                
                V = [V, R' * Vtilde];
            else
                R = blkdiag(msh.Omg{ip}.R,msh.Omg{ip}.R,msh.Omg{ip}.R);
                % V = [V, R'];% * eye(msh.Omg{ip}.nnode*3)];
                
                %if cnt ==  add the whole subdomain
                if (cnt == 0)
                    % xi = repmat(msh.Omg{ip}.xi,3,1);
                    
                    [lovlp,ial,ibl] = intersect(msh.Omg{ip-1}.nodes,msh.Omg{ip}.nodes);
                    % lovlp = setdiff(lovlp,msh.Omg{ip-1}.procbnd);
                    [rovlp,iar,ibr] = intersect(msh.Omg{ip+1}.nodes,msh.Omg{ip}.nodes);
                    % rovlp = setdiff(rovlp,msh.Omg{ip+1}.procbnd);
                    ovlp = union(lovlp,rovlp);
                    % procbnds = union(msh.Omg{ip-1}.procbnd,msh.Omg{ip+1}.procbnd);
                    subovlp = ovlp;%setdiff(ovlp,procbnds); % overlap between F and R without the R processor boundary. That will be contributed by the F subdomain
                    Rinterior = setdiff(msh.Omg{ip}.nodes,subovlp); % get ids of nodes shared between R and F subdomains
                    [~,Rinterior,~] = intersect(msh.Omg{ip}.nodes,Rinterior);
                    Rinterior = [Rinterior; Rinterior + msh.Omg{ip}.nnode; Rinterior + 2*msh.Omg{ip}.nnode];
                    
                    V = [V, R(Rinterior,:)'];
                    
                else % else add only interior. Also exclude processor boundary of the previous subdomain
                    xi = repmat(msh.Omg{ip}.xi,3,1);
                    [procbnd,ia,ib] = intersect(msh.Omg{ip-1}.procbnd,msh.Omg{ip}.nodes);
                    Rexterior1 = find(xi < 1);
                    Rexterior2 = [ib; ib + msh.Omg{ip}.nnode; ib + 2*msh.Omg{ip}.nnode];
                    Rexterior = union(Rexterior1,Rexterior2);
                    Rinterior = 1 : length(xi);
                    Rinterior(Rexterior) = [];
                    [~,~,ib] = intersect(msh.Omg{ip}.bnds.fixed,Rinterior);
                    msh.Omg{ip}.bnds.combined = ib;
                    V = [V, R(Rinterior,:)'];
                end
                % else add the whole thing
                cnt = cnt + 1;
            end
            
        end
    end
    
    % if (exist('lambdai'))
    %     bmodes = (size(V,2) - msh.m + 1);
    %     V(:,bmodes) = V(:,bmodes) * lambdai;
    % end

    % if (size(KL) == size(KNL))
    %     % K = V' * KL * V;%
    %    K = V' * (KL + KNL) * V; % sum of linear and nonlinear components of K
    % else
    %     % K = KL;% 
    %     K = KL + (V' * KNL * V);
    % end
    indx_j = repmat(1:msh.nedof,msh.nedof,1); indx_i = indx_j';
    Kindx.i = msh.e2g(:,indx_i(:)); Kindx.j = msh.e2g(:,indx_j(:));
    K = sparse(Kindx.i',Kindx.j',Ke_all);
    % K = V' * K * V;
    
    % K = V' * K * V;
    % u = V' * u;

    % R = (K * u);
    % Rf = R;%(free);
    
end